﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="OTDScriptBuilder._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">




    <div class="row" style="background-color: antiquewhite">
        <div class="col-md-4">
            <fieldset id="fsToolbar">

                <h3>Toolbar</h3>
                <div id="divToolBar" style="border:5px solid black; height:300px; vertical-align:top;background-color:#efefef;padding-left:15px;max-width:250px">
                    <a href="#" style="text-decoration:none" onclick="addNodeEdit()">Add Node</a>
                    <hr />
                    <h5>Actions</h5>
                     <a href="#" style="text-decoration:none" onclick="addAction('GoTo')">GoTo Node</a>
                    <br />
                    <a href="#" style="text-decoration:none" onclick="addAction('WebHook')">WebHook</a>
                    <hr />
                    <h5>Set Starting Action</h5>
                    <select id="ddlStartAction">
                        <option value=""></option>
                    </select>

                </div>
                <div id="divNodeToolBar" style="border:5px solid black; height:300px; vertical-align:top;background-color:#efefef;padding-left:15px; overflow-y: visible; overflow-x:visible; max-width:250px">
                    <div id="divElementContent" style="border-bottom:1px solid black; width:200px"><img src="img/Icon5.png" alt="Content Area" width ="200" height="40" /></div>
                    
            
                    <div id="divElementTextBox" style="border-bottom:1px solid black; width:200px"><img src="img/Icon1.png" alt="Text Box" width ="200" height="40" /></div>
                    
                 
                    <div id="divElementListBox" style="border-bottom:1px solid black; width:200px"><img src="img/Icon2.png" alt="List Box" width ="200" height="40" /></div>
                    
                 
                    <div id="divElementButton" style="border-bottom:1px solid black; width:200px"><img src="img/Icon4.png" alt="Button" width ="200" height="40" /></div>

              
                    <div id="divElementGrid" style="border-bottom:1px solid black; width:200px"><img src="img/Icon3.png" alt="Grid" width ="200" height="40" /></div>

               
                    <div id="divElementRadio" style="border-bottom:1px solid black; width:200px"><img src="img/Icon6.png" alt="Radio" width ="200" height="40" /></div>

                 
                    <div id="divElementCheckBox" style="border-bottom:1px solid black; width:200px"><img src="img/Icon7.png" alt="Check Box" width ="200" height="40" /></div>
                    <br />
 
                </div>

               <h3>Actions</h3>
                    <div style="border:5px solid black; height:200px;vertical-align:top;background-color:#efefef;padding-left:15px;max-width:250px">
                        <div id="divActionDesign" style="background-color:aliceblue"></div>
                    </div>

                <h3>Load Existing Script</h3>
                <select id="ddlExistingScripts">
                    <option value=""></option>
                </select>
                <button onclick="LoadScript();return false;">Load</button>
                <hr />
                

            </fieldset>

        </div>
        <div class="col-md-8">
              <h3>Designer <button onclick="location.reload();return false">Unload Script</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id="btnSaveScript" onclick="SaveScript();return false;">Save Script</button></h3>
            <table border="1" style ="border:outset; border-color:darkblue; width: 100%; border-width:thick; height:625px">
                <tr>
                    <td style="width:20%; vertical-align:top;text-align:center; background-color:black;color:white">
                        <h3>Nodes</h3>
                        <div id="divContentDesign" style="background-color:aliceblue"></div>
                        <div id="divNodeEdit" style="background-color:azure"><span id="lblNodeEdit"></span> - <a href="#" style="text-decoration:none" onclick="CancelEditNode()">Cancel Edit Node</a></div>
<%--                        <div style="border:5px solid black; height:600px;vertical-align:top;background-color:#f8d9d9;padding-left:15px">
                            <div id="divContentDesign" style="background-color:aliceblue"></div>
                            <div id="divNodeEdit" style="background-color:azure"><span id="lblNodeEdit"></span> - <a href="#" style="text-decoration:none" onclick="CancelEditNode()">Cancel Edit Node</a><div id="divNodeEditContent"></div></div>
       
                        </div>--%>
                    </td>
                    <td style="width:80%">
                        <div id="divBasicLayout"  style="width:100%; height:100%; border:dashed">
                            <table style="width:100%; height:100%" border="1">
                                <tr>
                                    <td style="vertical-align:top; text-align:center;height:100%">
                                        <h3>Content Area</h3>
                                        <div id="divBasicContentArea1" style="width:100%; height:100%"></div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divLeftPanelLayout"  style="width:100%; height:100%; border:dashed">
                            <table style="width:100%; height:100%" border="1">
                                <tr>
                                    <td style="vertical-align:top; text-align:center; width:30%" rowspan="2">
                                        <h3>Left Panel Content Area</h3>
                                        <div id="divLeftPanelContentArea1" style="width:100%; height:100%"></div>

                                    </td>
                                   <td style="vertical-align:top; text-align:center;width: 100%; height:50%">
                                       <h3>Content Area 1</h3>
                                        <div id="divLeftPanelContentArea2" style="width:100%; height:100%"></div>
                                    </td>
                                </tr>
                                <tr>
 
                                    <td style="vertical-align:top; text-align:center;width: 100%; height:50%">
                                        <h3>Content Area 2</h3>
                                        <div id="divLeftPanelContentArea3" style="width:100%; height:100%"></div>
                                    </td>
                            </tr>
               
                            </table>
                        </div>
                        <div id="divRightPanelLayout"  style="width:100%; height:100%;border:dashed">
                            <table style="width:100%; height:100%" border="1">
                                <tr>
                                    <td style="vertical-align:top; text-align:center; width:70%;height:50%">
                                        <h3>Content Area 1</h3>
                                        <div id="divRightPanelContentArea2" style="width:100%; height:100%"></div>

                                    </td>
                                   <td style="vertical-align:top; text-align:center;width: 30%; height:50%" rowspan="2">
                                       <h3>Right Panel Content Area</h3>
                                        <div id="divRightPanelContentArea1" style="width:100%; height:100%"></div>
                                    </td>
                                </tr>
                                <tr>
 
                                    <td style="vertical-align:top; text-align:center;height:100%">
                                        <h3>Content Area 2</h3>
                                        <div id="divRightPanelContentArea3" style="width:100%; height:100%"></div>
                                    </td>

                            </tr>
               
                            </table>
                        </div>
                    </td>
                </tr>
            </table>







        </div>
 
    </div>

    <div id="divEditContentAreaModal">
        <button value="DELETE" onclick="deleteElement()">DELETE</button>
        <hr />
        <textarea id="txtContentAreaText"></textarea>
        Element Order: <input type="text" id ="txtContentAreaOrder" />
        <hr />
        <button value="SAVE" onclick="SaveContentArea();return false;">SAVE</button>
    </div>

    <div id="divEdittextBoxModal">
        <button value="DELETE" onclick="deleteElement()">DELETE</button>
         <hr />
        Enter a Variable Name like this: FirstName.  This will create a variable name FirstName and can be used with a merge using a token like this: %FirstName%. These token values can be used to replace the value for the variable in any action condition or WebHook.
        <br />
        <input type="text" id ="txtTextBoxVariable" />
        <br />

         Enter a Label for the Text Box Box
         <br />
         <input type="text" id ="txtTextBoxLabel" />
        <br />
        Element Order: <input type="text" id ="txtTextBoxOrder" />
        <hr />
        <button value="SAVE" onclick="SavetextBox();return false;">SAVE</button>
    </div>

     <div id="divEditListBoxModal">
         <button value="DELETE" onclick="deleteElement();return false;">DELETE</button>
           <hr />
          Enter a Variable Name like this: FirstName.  This will create a variable name FirstName and can be used with a merge using a token like this: %FirstName%. These token values can be used to replace the value for the variable in any action condition or WebHook.
         <br />
         <input type="text" id ="txtListBoxVariable" />
         <br />

         Enter a Label for the List Box
         <br />
         <input type="text" id ="txtListBoxLabel" />
        <br />
        Element Order: <input type="text" id ="txtListBoxOrder" />
         <hr />
         To populate this List Box from an API call you can use a WebHook as an option.  If not, you can build it manually:
         <br />
         <a href="#" onclick="EditListBoxToggleWebHook(1)">Use Web Hook</a>
         <br />
         <div id="EditListBoxManual" class="col-md-12">
             <div class="col-md-4">
                 Display Text
                 <br />
                 <input type ="text" id ="txtListBoxDisplayAdd" />
                 Option Value
                 <br />
                 <input type ="text" id ="txtListBoxValueAdd" />
                 <br />
                 <button value="ADD" onclick="addListBoxOption()">ADD</button>
             </div>
             <div class="col-md-4">
                 <select id="ddlEditListBoxOptions">
                    <option value=""></option>
                 </select>
                 <input id="txtEditListBoxSelectedValue" type ="text" />
             </div>
             <div class="col-md-4">
                 <button value="REMOVE" onclick="RemoveListBoxOption();return false;">REMOVE</button>
             </div>

         </div>
        <div id="EditListBoxWebHook" class="col-md-12">
           
            <a href="#" onclick="EditListBoxToggleWebHook(0)">Close Web Hook Option</a>
            <h3>WebHook</h3>
            <select id="ddlNewSelectListWebHookType">
                <option value=""></option>
                <option value="GET">GET</option>
                <option value="POST">POST</option>
                <option value="PUT">PUT</option>
            </select>
            <br />
            End Point URL:<br /><input type="text" id="txtNewSelectListWebHookURL" />
            <br />
            Optional Body (JSON)<br /><textarea id="txtNewSelectListWebHoodBody"></textarea>
 
            <br />
                 Display Text field from JSON Response
                 <br />
                 <input type ="text" id ="txtListBoxDisplayWebHook" />
                  <br />
                 Option Value field from JSON Response
                 <br />
                 <input type ="text" id ="txtListBoxValueWebHook" />

        </div>

        

        <div class="col-md-12">
         <hr />
            <button value="SAVE" onclick="SaveListBox();return false;">SAVE</button>
        </div>
        
    </div>

    <div id="divEditButtonModal">
        <button value="DELETE" onclick="deleteElement();return false;">DELETE</button>
          <hr />
        Button Text: <input type="text" id="txtNewButtonText" />
        <br />
        Please configure your Button ACTION: 
        <select id ="ddlNewButtonAction">
          
        </select>
        <br />
        Element Order: <input type="text" id ="txtButtonOrder" />
        <hr />

        <button value="SAVE" onclick="saveNewButton();return false;">SAVE</button>
    </div>

    <div id="divEditGridModal">
        <button value="DELETE" onclick="deleteElement();return false;">DELETE</button>
          <hr />

            <h3>WebHook</h3>
            <select id="ddlNewGridWebHookType">
                <option value=""></option>
                <option value="GET">GET</option>
                <option value="POST">POST</option>
                <option value="PUT">PUT</option>
            </select>
            <br />
            End Point URL:<br /><input type="text" id="txtNewGridWebHookURL" />
            <br />
            Optional Body (JSON)<br /><textarea id="txtNewGridWebHookBody"></textarea>
            <br />
            Value Field:
            <input type="text" id="txtNewGridValueField" />
            <h3>Columns</h3>
        <table>
            <tr>
                <td style="text-align:left; width:75%">
                    Column Header text: <input type="text" id="txtNewGridHeaderText" />
                    <br />
                    Column value: <input type="text" id="txtNewGridHeaderValue" />
                   <br />

                    Visible:
                    <select id="ddlNewGridColumnVisible">
                        <option value="1">TRUE</option>
                        <option value="0">FALSE</option>
                    </select>
                    <br />
                    DataType:
                        <select id="ddlNewGridColumnType">
                        <option value="System.String">string</option>
                        <option value="System.Int32">int</option>
                        <option value="System.DateTime">DateTime</option>
                        <option value="System.Decimal">decimal</option>
                        <option value="System.Int64">long</option>
                        <option value="System.Single">float</option>

                    </select>
                </td>
                <td>
                    <button id="btnAddGridColumn" onclick="addGridcolumn();return false">Add Column</button>
                    <hr />
                    <button id="btnClearGridColumn" onclick="ClearGridColumns();return false">Clear Grid Columns</button>
                </td>
            </tr>
        </table>

  
            <br />
            <span id="lblEditGridColumnDisplay"></span>
        <br />
        Element Order: <input type="text" id ="txtGridOrder" />
        <hr />

        <input type="hidden" id="hdnGridColumns" />
        <button value="SAVE" onclick="saveNewGrid();return false;">SAVE</button>
    </div>

    <div id="divEditActionGoTo">
    <h3>Edit Action GoTo Node</h3>
        <span id="lblConditionalActions"></span>
        <hr />
        Condition:<br />
        <input type="text" value="1" id="txtActionCondition1" />
        <select id="ddlActionCondition">
            <option value="equal">Equal</option>
            <option value="notequal">Not Equal</option>
            <option value="greater">Greater Than</option>
            <option value="less">Less Than</option>
        </select>
        <input type="text" value="1" id="txtActionCondition2" />
            <select id="ddlActionConditionType">
            <option value="int">int</option>
            <option value="long">long</option>
            <option value="decimal">decimal</option>
            <option value="DateTime">DateTime</option>
            <option value="string">string</option>
        </select>
    <select id="ddlEditAtionNodes">
        <option value=""></option>
    </select><br />
        Indicate the type so we understand how to make the comparrison. You can make any number of conditions each going to anoteher Node.
    <hr />
         <button value="ADD" onclick="addActionGoTo();return false;">ADD</button>
        <hr />
        <button value="SAVE" onclick="saveActionGoTo();return false;">SAVE</button>
        <input type="hidden" id="hdnActionValues" />
    </div>

    <div id="divEditActionWebHook">
            <h3>Edit Action WebHook</h3>
            Please select type:
            <select id="ddlActionWebHookType">
                <option value="GET">GET</option>
                <option value="POST">POST</option>
                <option value="PUT">PUT</option>
            </select>
        <br />
            End Point URL:<br /><input type="text" id="txtActionWebHookURL" />
            <br />
            Optional Body (JSON)<br /><textarea id="txtActionWebHoodBody"></textarea>
            <br />
            Optional Response (Use comma to seperate returned values as Variable Names)<br /><textarea id="txtActionWebHoodResponse"></textarea>
            <br />
            If you are getting a response back and may need the values for conditional actions or WebHook API calls or to set values for Variables that may update values on the form, we will need the Respose defined as JSON.
            *NOTE: You can use Merge by adding Tags like %FirstName% anywhere in the Body or URL and the Variable values will be merged. QueryString Values are automatically available for merge as variables were already created.
           <br /><br /> 
        Chose Next Action:
        <select id="ddlActionNextAction">
                <option value=""></option>
            </select>

            <hr />
        <button value="SAVE" onclick="saveActionWebHook();return false;">SAVE</button>
    </div>

    <div id="divAddNodeModal">
        Please enter a Node Name
          <br />
        <input type="text" id="txtAddNodeName" />
        <br />
        <select id="ddlNewNodeTemplate">
            <option value="1">Basic (Single Content Area)</option>
            <option value="2">Left Panel (3 Contnet areas)</option>
            <option value="3">Right Panel (3 Content Areas)</option>
        </select>
        <br />
        <button value="ADD" onclick="handleAddNode();return false;">Add</button>
    </div>

    <div id="divAddActionModal">
        Please enter an Action Name
          <br />
        <input type="text" id="txtAddActionName" />
        <input type="hidden" id="AddActionType" />
        <br />
        <button value="ADD" onclick="addActionHandler();return false;">Add</button>
    </div>

        <div id="divAddScriptModal">
        Please enter a Name for the script
          <br />
        <input type="text" id="txtAddScriptName" />
        <br />
        <button value="ADD" onclick="handleAddScriptName();return false;">Add</button>
            <hr />
        <span id="lblAddScriptMessage" style="color:maroon; font-weight:bold"></span>
    </div>

    <div id="divMessageModal">
    <span id="lblMessage"></span>
    </div>


<script type="text/javascript">
    var baseAPIURL = "";
    var scriptId = -1;
    var editMode = 0;
    var nodeCount = 0;
    var selectedNode = 0;
    var selectedAction = 0;
    var selectedElement = 0;
    var listBoxEditMode = 0;
    var nodes = [];
    var nodeContent1 = [];
    var nodeContent2 = [];
    var nodeContent3 = [];
    var actions = [];
    var variables = [];


    function addActionGoTo() {
        var isValid = false;
        if ($('#ddlEditAtionNodes').val() != null) {
            if ($('#ddlEditAtionNodes').val() != "") {
                var actionValue = $('#hdnActionValues').val();
                if (actionValue.length > 0) {
                    actionValue = actionValue + "|";
                }
                actionValue =  actionValue + $('#txtActionCondition1').val() + ":" + $('#ddlActionCondition').val() + ":" + $('#txtActionCondition2').val() + ":" + $('#ddlActionConditionType').val() + ":" + $('#ddlEditAtionNodes').val();
                $('#hdnActionValues').val(actionValue);
                var labelvalue = $('#lblConditionalActions').html();
                if (labelvalue.length > 0) {
                    labelvalue = labelvalue + "<br />";
                }
                labelvalue = labelvalue + $('#txtActionCondition1').val() + " " + $('#ddlActionCondition').val() + " " + $('#txtActionCondition2').val() + " type: " + $('#ddlActionConditionType').val() + " node: " + $('#ddlEditAtionNodes').val();
                $('#lblConditionalActions').html(labelvalue);
                isValid = true;
            }

        }

        if (isValid != true) {
            alert("No Node Selected!");
        }


    }



    function clearScript() {
        editContainer = "";
        editMode = 0;
        nodeCount = 0;
        selectedNode = 0;
        selectedAction = 0;
        selectedElement = 0;
        listBoxEditMode = 0;
        nodes = [];
        actions = [];
        variables = [];
        $('#divContentDesign').empty();
        $('#divActionDesign').empty();
        $('#ddlStartAction').empty();

    }


    function handleAddScriptName() {
        var scriptName = $("#txtAddScriptName").val();
        var entityId = $("#MainContent_txtEntityId").val();

        if (scriptName.length == 0) {
            $("#lblAddScriptMessage").html("Please provide a name for the script");
        }
        else {
            $("#lblAddScriptMessage").html("");
                // call API
            var url = baseAPIURL + 'AdminScript/SaveScript?scriptName=' + scriptName + '&entityId=' + entityId;
    
          $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: url,
              success: function (result) {
                        if (result == -1) {
                            $("#lblAddScriptMessage").html("There is already a script named " + scriptName + ". Please use a different name.");
                        }
                        else {
                            scriptId = result;
                           
                            $("#divAddScriptModal").dialog('close');
                            loadExistingScripts();
                            ProcessSaveScript();

                        }
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });


        }

    }

    function SaveScript() {
        if (scriptId == -1) {
            $("#lblAddScriptMessage").html("");
            $("#txtAddScriptName").val("");
            $("#divAddScriptModal").dialog('open');
        }
        else {
            var url = baseAPIURL + 'AdminScript/ClearScript?scriptId=' + scriptId;
             $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: url,
               success: function (result) {
                 ProcessSaveScript();

                },
                failure: function (errMsg) {
                   alert('Error Clearing Script Data!')
                }
            });
            
        }


    }

    function saveElement(elementTypeId,nodeId,elementOrder,parameters,isLast) {
                var url2 = baseAPIURL + 'AdminElement/SaveElement?elementTypeId=' + elementTypeId + '&nodeId=' + nodeId + "&ElementOrder=" + elementOrder;
          
                    $.ajax({
                    type: 'POST',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: url2,
                    success: function (result) {
                            
                        var elementId = result;
    
                        $("#lblMessage").html("<h3>Saving Script. Please wait ...</h3><p>adding element paramters </p>");
                            var url3 = baseAPIURL + 'AdminElementParamters/SaveElementParameter';
                        $.ajax({
                            type: 'POST',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            url: url3,
                            data: JSON.stringify({"parameterValues": parameters,"elementId":elementId}),
                            success: function (result) {
                                if (isLast == true)
                                {
                                    CancelEditNode();
                                }

                            },
                            failure: function (errMsg) {
                                alert('Error creating Element Parameters!')
                            }
                        });

                    },
                    failure: function (errMsg) {
                        alert('Error creating Element!')
                    }
                });

    }

    function processSaveElements(containerID,nodeId) {

        var elementOrder = 0;
        var elementIndex = -1;
        var content = "";
        // Load the Container


                var eTop = ($('#' + containerID + ' > div').length)-1;

                $('#' + containerID + ' > div').each(function () {
                elementIndex = elementIndex + 1;
                var elementType = $(this).attr("type");
                var elementParameters = "";
                var ElementDiv = $(this);

                ElementDiv.find('input').each(function () {
                    elementParameters = $(this).val();
                });
                var elementTypeId;
                    if (elementType == "ContentArea") {
                        if (elementParameters.length > 0) {
                            var values = elementParameters.split("|");
                            elementOrder = values[2];
                        }
                    elementTypeId = 1;
                           
                }
                    if (elementType == "TextBox") {
                        if (elementParameters.length > 0) {
                            var values = elementParameters.split("|");
                            elementOrder = values[2];
                        }
                    elementTypeId = 2;
                }
                    if (elementType == "ListBox") {
                        if (elementParameters.length > 0) {
                            var values = elementParameters.split("|");
                            if (values[0] == 0) {
                                elementOrder = values[4];
                            }
                            else {
                                elementOrder = values[8];
                            }
                               
                        }
                    elementTypeId = 3;
                }
                    if (elementType == "Button") {
                        if (elementParameters.length > 0) {
                            var values = elementParameters.split("|");
                            elementOrder = values[2];
                        }
                    elementTypeId = 4;
                    }
                    if (elementType == "Grid") {
                        if (elementParameters.length > 0) {
                            var values = elementParameters.split("|");
                            elementOrder = values[3];
                        }
                        elementTypeId = 5;
                        
                       
                }

                    if (elementOrder == 0) {
                        elementOrder = elementIndex;
                    }

                    $("#lblMessage").html("<h3>Saving Script. Please wait ...</h3><p>adding element " + elementType + "</p>");
                    var isLast = (eTop == elementIndex);
                    saveElement(elementTypeId,nodeId,elementOrder,elementParameters,isLast)

            });
           return true;
    }


    function createNode(nodeName,TId,nodeIndex) {
            $("#lblMessage").html("<h3>Saving Script. Please wait ...</h3><p>adding node " + nodeName + "</p>");
             var url = baseAPIURL + 'AdminNode/SaveNode?scriptId=' + scriptId + '&nodeName=' + nodeName + '&nodeTemplateId=' + TId;
             $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: url,
                 success: function (result) {
                     var nodeId = result;
                       nodeSelect(nodeIndex + 1);

                  
                     if (TId == 1) {
                         processSaveElements("divBasicContentArea1", nodeId,nodeIndex);
                     }
                     if (TId == 2) {

                         saveProcessComplete = processSaveElements("divLeftPanelContentArea1", nodeId);
                         saveProcessComplete = processSaveElements("divLeftPanelContentArea2", nodeId);
                         saveProcessComplete = processSaveElements("divLeftPanelContentArea3", nodeId);
                  
                     }
                     if (TId == 3) {
           
                         saveProcessComplete = processSaveElements("divRightPanelContentArea1", nodeId);
                         saveProcessComplete = processSaveElements("divRightPanelContentArea2", nodeId);
                         saveProcessComplete = processSaveElements("divRightPanelContentArea3", nodeId);
                     }
                     
                },
                 failure: function (errMsg) {
                     alert('failed');
                    $("#lblMessage").html("Create Node Failed: " + errMsg);
                }
            });
    }

    function saveStartAction() {
                if ($("#ddlStartAction").val() != null) {
                    if ($("#ddlStartAction").val().length > 0) {
                        
                        $("#lblMessage").html("<h3>Saving Script. Please wait ...</h3><p>Setting Start action " + $("#ddlStartAction").val() + "</p>");
                        var url5 = baseAPIURL + 'AdminStartAction/SaveStartAction?scriptId=' + scriptId + '&actionName=' + $("#ddlStartAction").val();
                        $.ajax({
                        type: 'POST',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                            url: url5,
                        success: function (result) {
                      
                         
                        },
                        failure: function (errMsg) {
                        alert('Error creating Starting Action!')
                        }
                        });
                    }
                }
    }

    function saveAction(actionName,actionTypeId,actionParameters) {
        $("#lblMessage").html("<h3>Saving Script. Please wait ...</h3><p>adding action " + actionName + "</p>");
        var url4 = baseAPIURL + 'AdminAction/SaveAction';
        $.ajax({
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: url4,
            data: JSON.stringify({ "parameterValues": actionParameters,"scriptId": scriptId,"actionName":actionName,"actionTypeId":actionTypeId}),
                success: function (result) {
                    if (actionName == $("#ddlStartAction").val())
                    {
                            saveStartAction();
                    }        
                  

        },
        failure: function (errMsg) {
        alert('Error creating Action!')
        }
        });
    }

     function ProcessSaveScript() {
        $("#lblMessage").html("<h3>Saving Script. Please wait ...</h3>");
        $("#divMessageModal").dialog('open');

         var nodeIndex=-1;

        //Create Each Node
         $('#divContentDesign > div').each(function () {
             nodeIndex = nodeIndex + 1;
             var nodeName = $(this).attr("nodeName");
             var nodeDiv = $(this);
             var nodeDivID = $(this).attr("id");
             var TId = $(this).attr("TemplateId");

              createNode(nodeName,TId,nodeIndex)


         });
        // Create Each Node Element

        // Create Each Action


        $('#divActionDesign > div').each(function () {
        var actionName = $(this).attr("actionName");
        var actionType = $(this).attr("type");
        var actionDiv = $(this);
        var actionParameters = "";
            actionDiv.find('input').each(function () {
                actionParameters = $(this).val();
                                              
            });

        var actionTypeId;
        if (actionType == "GoTo") {
            actionTypeId = 1;
        }
        if (actionType == "WebHook") {
            actionTypeId = 2;
        }

        saveAction(actionName, actionTypeId, actionParameters);
    });

         
        // Set Start Action
         

         $("#divMessageModal").dialog('close');
    }

    function addNode(NodeName,TemplateId) {
        nodes[nodes.length] = NodeName;
        nodeCount = nodeCount + 1;
        var nodeHTML = "<div TemplateId='" + TemplateId + "' nodeName='" + NodeName + "' id='div_node_" + nodeCount + "' onclick='nodeSelect(" + nodeCount + ")' style='color: black;border:1px solid black; width=200px'>node" + nodeCount + " " + NodeName +  "</div>";
        $('#divContentDesign').append(nodeHTML);

        nodeContent1[nodes.length - 1] = "";
        nodeContent2[nodes.length - 1] = "";
        nodeContent3[nodes.length - 1] = "";

    }

    function addNodeEdit() {
        $("#divAddNodeModal").dialog('open');
    }


    function handleAddNode() {
        var nodeName = $('#txtAddNodeName').val();
        var TId = $('#ddlNewNodeTemplate').val();
        if (nodeName.length == 0) {
            alert("Please enter a Node Name");
        }
        else {
            var i;
            var isFound = false;
            for (i = 0; i < nodes.length; i++) {
                if (nodes[i] == nodeName) {
                    isFound = true;
                }
            
            }

            if (isFound == true) {
                alert("Node " + nodeName + " already exist")
            }
            else {
                addNode(nodeName,TId);
                $('#txtAddNodeName').val("");
                $("#divAddNodeModal").dialog('close');
            }

        }
    }
    function nodeSelect(nodeNumber) {
        selectedNode = nodeNumber;

        $('#divToolBar').hide();
        //$('#lblNodeEdit').html("Edit node" + nodeNumber);
        $('#divNodeToolBar').show();
        $('#divNodeEdit').show();
        $('#divContentDesign').hide();


        var tId = $('#div_node_' + selectedNode).attr("TemplateId");

        if (tId == 1) {
            $('#divBasicLayout').show();
            $('#divLeftPanelLayout').hide();
            $('#divRightPanelLayout').hide();

            $('#divBasicContentArea1').html(nodeContent1[nodeNumber - 1]);
        }
        if (tId == 2) {
            $('#divBasicLayout').hide();
            $('#divLeftPanelLayout').show();
            $('#divRightPanelLayout').hide();

            $('#divLeftPanelContentArea1').html(nodeContent1[nodeNumber - 1]);
            $('#divLeftPanelContentArea2').html(nodeContent2[nodeNumber - 1]);
            $('#divLeftPanelContentArea3').html(nodeContent3[nodeNumber - 1]);
        }
        if (tId == 3) {
            $('#divBasicLayout').hide();
            $('#divLeftPanelLayout').hide();
            $('#divRightPanelLayout').show();

            $('#divRightPanelContentArea1').html(nodeContent1[nodeNumber - 1]);
            $('#divRightPanelContentArea2').html(nodeContent2[nodeNumber - 1]);
            $('#divRightPanelContentArea3').html(nodeContent3[nodeNumber - 1]);
        }

        //$('#divNodeEditContent').html($('#div_node_' + selectedNode).html());

        editMode = 1;
    }



    function CancelEditNode() {
        $('#divContentDesign').show();
        $('#divNodeEdit').hide();
        $('#divToolBar').show();
        $('#divNodeToolBar').hide();

        $('#divBasicLayout').hide();
        $('#divLeftPanelLayout').hide();
        $('#divRightPanelLayout').hide();
        editMode = 0;
    }


    function EditElement(index, containerId) {
        if (editMode == 1) {
            editContainer = containerId;
            selectedElement = index;
            var settings = "";
            var childIndex = -1;
            var value = "";
            $('#' + containerId + ' > div').each(function() {
                childIndex = childIndex + 1;
                if (childIndex == index) {
                   $(this).find('input').each(function (t) {
                   value = $(this).val().replace(/-S-/g,"'");
                   });
                    if ($(this).attr("type") == "ContentArea") {
                            $('#txtContentAreaText').val("");
                            $('#txtContentAreaOrder').val("");
                        if (value.length > 0) {
                            var values = value.split("|");
                            $('#txtContentAreaText').jqteVal(values[1]);
                            $('#txtContentAreaOrder').val(values[2]);
                        }

                        $("#divEditContentAreaModal").dialog('open');
                    }
                    if ($(this).attr("type") == "TextBox") {
                        $('#txtTextBoxVariable').val("");
                        $('#txtTextBoxLabel').val("");
                        $('#txtTextBoxOrder').val("");
                        if (value.length > 0) {
                            var values = value.split("|");
                            $('#txtTextBoxVariable').val(values[0]);
                            $('#txtTextBoxLabel').val(values[1]);
                            $('#txtTextBoxOrder').val(values[2]);
                        }
                        $("#divEdittextBoxModal").dialog('open');
                    }
                    if ($(this).attr("type") == "ListBox") {
                        $('#txtListBoxVariable').val("");
                        $('#ddlEditListBoxOptions').empty();
                        $('#txtEditListBoxSelectedValue').val("");

                        $('#txtNewSelectListWebHookURL').val("");
                        $('#txtNewSelectListWebHoodBody').val("");
                        $('#txtNewSelectListWebHoodResponse').val("");
                        $('#txtListBoxDisplayWebHook').val("");
                        $('#txtListBoxValueWebHook').val("");
                        $('#txtListBoxLabel').val("");
                        


                        if (value.length > 0) {
                            var values = value.split("|");
                            $('#txtListBoxVariable').val(values[1]);
                            $('#txtListBoxLabel').val(values[2]);
                            $('#txtListBoxOrder').val(values[4]);
                            if (values[0] == "0") {
                                EditListBoxToggleWebHook(0);
                                var listOptions = values[3].split(",");
                                var i;
                                for (i = 0; i < listOptions.length; i++) {
                                    var li = listOptions[i].split(":");
                                    $('#ddlEditListBoxOptions').append(new Option(li[0], li[1], false, false));
                                }
                            }
                            else {
                                EditListBoxToggleWebHook(1);
                                $('#ddlNewSelectListWebHookType').val(values[3]);
                                $('#txtNewSelectListWebHookURL').val(values[4]);
                                $('#txtNewSelectListWebHoodBody').val(values[5]);
                                $('#txtListBoxDisplayWebHook').val(values[6]);
                                $('#txtListBoxValueWebHook').val(values[7]);
                                $('#txtListBoxOrder').val(values[8]);
                            }


  

                        }
                        $("#divEditListBoxModal").dialog('open');
                    }
                    if ($(this).attr("type") == "Button") {
                        $('#txtNewButtonText').val("");
                        var buttonParams = value.split("|");
                        $('#ddlNewButtonAction').empty();
                         for (i = 0; i < actions.length; i++) {
                             $('#ddlNewButtonAction').append(new Option(actions[i],actions[i], false, false));
                        }
                        $('#ddlNewButtonAction').val(buttonParams[1]);
                        $('#txtNewButtonText').val(buttonParams[0]);
                        $('#txtButtonOrder').val(buttonParams[2]);
                        
                        $("#divEditButtonModal").dialog('open');
                    }
                    if ($(this).attr("type") == "Grid") {

                        $('#ddlNewGridWebHookType').val("");
                        $('#txtNewGridWebHookURL').val("");
                        $('#txtNewGridWebHookBody').val("");
                        $('#txtNewGridHeaderText').val("");
                        $('#txtNewGridHeaderValue').val("");
                        $('#ddlNewGridColumnVisible').val("");
                        $('#ddlNewGridColumnType').val("");
                        $('#txtGridOrder').val("");
                        $('#lblEditGridColumnDisplay').html("");
                        $('#hdnGridColumns').val("");
                        $('#txtNewGridValueField').val("");
                        if (value.length > 0)
                        {
                            var values = value.split("|");

                            $('#ddlNewGridWebHookType').val(values[0]);
                            $('#txtNewGridWebHookURL').val(values[1]);
                            $('#txtNewGridWebHookBody').val(values[2]);
                            $('#txtGridOrder').val(values[3]);
                            $('#txtNewGridValueField').val(values[4]);
                            $('#hdnGridColumns').val(values[5]);

                           var displayValue = "";

                           var columnValues = values[5].split(",");
                           var i;
                            for (i = 0; i < columnValues.length; i++) {
                                var cv = columnValues[i].split(":");
                                if (displayValue.length > 0)
                                {
                                        displayValue = displayValue + "<br />";
                                }
                                var visibleDisplay = "";
                                if (cv[2] == 1)
                                {
                                    visibleDisplay = "True";
                                }
                                else
                                {
                                    visibleDisplay = "False";
                                }
                                displayValue = displayValue + "Column Header: " + cv[0] + " Value Field: " + cv[1] + " Visible: " + visibleDisplay + " Type: " + cv[3];
                            }

                             $('#lblEditGridColumnDisplay').html(displayValue);
                        }


                        $("#divEditGridModal").dialog('open');
                    }//end for
                 } // end if grid
            });// End each node

        }// End if edit
        
    }// end function

    function deleteElement() {

        var childIndex = -1;
        var v = "";
        var delVariable = "";

            $('#' + editContainer + ' > div').each(function() {
                childIndex = childIndex + 1;
                if (childIndex == selectedElement) {
                     $(this).find('input').each(function (t) {
                         v = $(this).val();

                        if ($(this).attr("type") == "TextBox") {
                            delVariable = v;
                        }
                         if ($(this).attr("type") == "ListBox") {
                             if (v.length > 0) {
                                var vList = v.split("|");
                                delVariable = vList[1];
                             }

                        }

                    });
                    $(this).remove();
                }

                if (delVariable.length > 0) {
                    variables = jQuery.grep(variables, function(value) {
                    return value != delVariable;
                    });
                }

                $("#divEditContentAreaModal").dialog('close');
                $("#divEdittextBoxModal").dialog('close');
                $("#divEditListBoxModal").dialog('close');
                $("#divEditButtonModal").dialog('close');
        });
        var contentHTML = "";
        childIndex = -1;
   


        $('#' + editContainer + ' > div').each(function() {
            var type = "";
            var value = "";
            var typeAttribute = "";
            var myID = $(this).attr("id");
             var img = "";
            childIndex = childIndex + 1;
                    if ($(this).attr("type") == "ContentArea") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if ($(this).attr("type") == "TextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if ($(this).attr("type") == "ListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if ($(this).attr("type") == "Button") {
                        type = "Button";
                        typeAttribute = "Button";
                         img = "img/Icon4.png";
                    }
                   if ($(this).attr("type") == "Grid") {
                       type = "Grid";
                       typeAttribute = "Grid";
                       img = "img/Icon3.png";
                    }

            $(this).find('input').each(function (t) {
                   value = $(this).val();
                   });
            contentHTML = contentHTML + "<div type='" + typeAttribute + "' containerId='" + editContainer + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childIndex + ",\"" + editContainer + "\")'>" + (childIndex+1) + " <img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" /> <input type='hidden' value='" + value.replace("'","&#39;") + "'  /></div>";
        });

        $('#' + editContainer).html(contentHTML);

        if (editContainer.indexOf("1") >= 0) {
            nodeContent1[selectedNode - 1] = contentHTML;
        }
        if (editContainer.indexOf("1") >= 0) {
            nodeContent2[selectedNode - 1] = contentHTML;
        }
        if (editContainer.indexOf("1") >= 0) {
            nodeContent3[selectedNode - 1] = contentHTML;
        }

    }

    function addAction(actionType) {
        $("#AddActionType").val(actionType);
        $("#divAddActionModal").dialog('open');
    }

    function addActionHandler() {
        var actionName = $('#txtAddActionName').val();
        var actionType = $('#AddActionType').val();
        if (actionName.length == 0) {
            alert("Please enter an Action Name");
        }
        else {
            var i;
            var isFound = false;
            for (i = 0; i < actions.length; i++) {
                if (actions[i] == actionName) {
                    isFound = true;
                }
            
            }

            if (isFound == true) {
                alert("Action " + actionName + " already exist")
            }
            else {
                addActionProcess(actionType,actionName);
                $('#txtAddActionName').val("");
                $('#AddActionType').val("");
                $("#divAddActionModal").dialog('close');
            }

        }
    }

    function addActionProcess(actionType, actionName) {
        actions[actions.length] = actionName;
        var value = "";
        var typeAttribute = actionType;

        var contentHTML = "";
        contentHTML = "<div id='div_action_" + (actions.length-1) + "' actionName='" + actionName + "' type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditAction(" + (actions.length-1) + ")'>" + (actions.length) + " " + actionName + ": " + actionType + "<input type='hidden' value='" + value + "'  /></div>";

        $('#divActionDesign').append(contentHTML);

        $('#ddlStartAction').append(new Option(actionName, actionName, false, false));
    }

    function EditAction(index) {
        selectedAction = index;
        var actionDiv = $('#div_action_' + index);
        var actionType = actionDiv.attr("type");
        var actionName = actionDiv.attr("actionName");
        var paramerters = "";

        actionDiv.find('input').each(function (t) {
        paramerters = $(this).val();       
        });

        if (actionType == "GoTo") {
            $('#ddlEditAtionNodes').empty();
            $('#hdnActionValues').empty();
            for (i = 0; i < nodes.length; i++) {
                $('#ddlEditAtionNodes').append(new Option(nodes[i],nodes[i], false, false));
            }
            //$('#ddlEditAtionNodes').val(paramerters);

            var parmOptions = paramerters.split("|");
            var i;
            var displayValues = "";
            for (i = 0; i < parmOptions.length; i++) {
                
                if (displayValues.length > 0) {
                    displayValues = displayValues + "<br />";
                }
                if (parmOptions[i].length > 0) {
                    var v = parmOptions[i].split(":");
                    displayValues = displayValues + v[0] + " " + v[1] + " " + v[2] + " Node: " + v[4] + " Type " + v[3];
                    $('#ddlEditAtionNodes').val(v[4]);
                }

            }

            $('#lblConditionalActions').html(displayValues);
            $('#hdnActionValues').val(paramerters);
            $('#divEditActionGoTo').dialog('open');
        }
        if (actionType == "WebHook") {
            var params = paramerters.split("|");
            $('#ddlActionWebHookType').val(params[0]);
            $('#txtActionWebHookURL').val(params[1]);
            $('#txtActionWebHoodBody').val(params[2]);
            $('#txtActionWebHoodResponse').val(params[3]);
            
            $('#ddlActionNextAction').empty();
            for (i = 0; i < actions.length; i++) {
                if (i != (index)) {
                    $('#ddlActionNextAction').append(new Option(actions[i],actions[i], false, false));
                }
            }

            $('#ddlActionNextAction').val(params[4]);
             
            $('#divEditActionWebHook').dialog('open');
        }
    } 

    function saveActionGoTo() {
        var actionDiv = $('#div_action_' + selectedAction);
        var paramerters = $('#hdnActionValues').val();

        actionDiv.find('input').each(function (t) {
        $(this).val(paramerters);         
        });

        $('#ddlEditAtionNodes').val("");
        $('#divEditActionGoTo').dialog('close');
    }

    function saveActionWebHook() {
        var actionDiv = $('#div_action_' + selectedAction);
        var paramerters = $('#ddlActionWebHookType').val();
        paramerters = paramerters + "|" + $('#txtActionWebHookURL').val();
        paramerters = paramerters + "|" + $('#txtActionWebHoodBody').val();
        paramerters = paramerters + "|" + $('#txtActionWebHoodResponse').val();
        paramerters = paramerters + "|" + $('#ddlActionNextAction').val();

        actionDiv.find('input').each(function (t) {
        $(this).val(paramerters);       
        });

        $('#ddlActionWebHookType').val("");
        $('#txtActionWebHookURL').val("");
        $('#txtActionWebHoodBody').val("");
        $('#txtActionWebHoodResponse').val("");
        $('#ddlActionNextAction').val("");


       $('#divEditActionWebHook').dialog('close');
    }        

    function SaveContentArea() {
        if ($('#txtContentAreaText').val() == "") {
            alert("Please provide content");
        }
        else {
            if ($('#txtContentAreaText').val().indexOf("|") > -1) {
                alert("'|' is a reserved character");
            }
            else {

                if (isNumeric($('#txtContentAreaOrder').val()) == false) {
                    alert("Order must be a number between 1 and 40");
                }
                else {
                    var elementIndex = -1;
                    $('#' + editContainer + ' > div').each(function () {
                        elementIndex = elementIndex + 1;
                        $(this).find('input').each(function (t) {
                            if (selectedElement == elementIndex) {
                                $(this).val("" + "|" + $('#txtContentAreaText').val().replace(/'/g,"-S-") + "|" + $('#txtContentAreaOrder').val() + "|" + editContainer);
                            }
                   
                        });
                    });
                    $("#divEditContentAreaModal").dialog('close');
                }

            }

        }

        if (editContainer.indexOf("1") >= 0) {
            nodeContent1[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("2") >= 0) {
            nodeContent2[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("3") >= 0) {
            nodeContent3[selectedNode-1] = $('#' + editContainer).html();
        }
    }

    function SavetextBox() {
        if ($('#txtTextBoxVariable').val() == "" || $('#txtTextBoxLabel').val() == "") {
            alert("Please provide a Variable Name to store the value and a Label value");
        }
        else {
            if ($('#txtTextBoxVariable').val().indexOf("|") >= 0 || $('#txtTextBoxLabel').val().indexOf("|") >= 0) {
                alert("| is a reserved character");
            }
            else {
                if (isNumeric($('#txtTextBoxOrder').val()) == false) {
                    alert("Order must be a number between 1 and 40");
                }
                else {
                    var isfound = false;
                    var i;
                        for (i = 0; i < variables.length; i++) {
                            if (variables[i] == $('#txtTextBoxVariable').val()) {
                                isfound = true;
                            }
                        }

   
                        if (isfound == false) {
                            variables[variables.length] = $('#txtTextBoxVariable').val();
                        }
                        var elementIndex = -1;
                        $('#' + editContainer + ' > div').each(function () {
                            elementIndex = elementIndex + 1;
                            $(this).find('input').each(function (t) {
                                if (selectedElement == elementIndex) {
                                    $(this).val($('#txtTextBoxVariable').val().replace(/'/g,"-S-") + "|" + $('#txtTextBoxLabel').val().replace(/'/g,"-S-") + "|" + $('#txtTextBoxOrder').val() + "|" + editContainer);
                                }
                   
                            });
                        });
                        $("#divEdittextBoxModal").dialog('close');
                }
            }

            }
        if (editContainer.indexOf("1") >= 0) {
            nodeContent1[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("2") >= 0) {
            nodeContent2[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("3") >= 0) {
            nodeContent3[selectedNode-1] = $('#' + editContainer).html();
        }
    }

    function saveNewButton() {
        if ($('#ddlNewButtonAction').val() == null || $('#txtNewButtonText').val().length == 0) {
            alert("Please provide Text for the Button and SELECT an action");
        }
        else {

            if ($('#txtNewButtonText').val().indexOf("|") >= 0) {
                alert("| is a reserved character");
            }
            else {
                if (isNumeric($('#txtButtonOrder').val()) == false) {
                    alert("Order must be a number between 1 and 40");
                }
                else {
                    var buttonParameters = $('#txtNewButtonText').val().replace(/'/g,"-S-");
                    buttonParameters = buttonParameters + "|" + $('#ddlNewButtonAction').val() + "|" + $('#txtButtonOrder').val() + "|" + editContainer;
                        var elementIndex = -1;
                        $('#' + editContainer + ' > div').each(function () {
                            elementIndex = elementIndex + 1;
                            $(this).find('input').each(function (t) {
                                if (selectedElement == elementIndex) {
                                    $(this).val(buttonParameters);
                                }
                   
                            });
                        });
                  $("#divEditButtonModal").dialog('close');
                }
            }


        }
        if (editContainer.indexOf("1") >= 0) {
            nodeContent1[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("2") >= 0) {
            nodeContent2[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("3") >= 0) {
            nodeContent3[selectedNode-1] = $('#' + editContainer).html();
        }
    }

    function saveNewGrid() {
        if ($('#txtNewGridWebHookURL').val() == null || $('#hdnGridColumns').val().length == 0 || $('#txtNewGridValueField').val().length == 0) {
            alert("Please Provide a WebHook URL Web Hook Type,Value Field, and define all Grid Columns");
        }
        else {

            if ($('#txtNewGridWebHookURL').val().indexOf("|") >= 0 || $('#txtNewGridValueField').val().indexOf("|") >= 0 || $('#txtNewGridWebHookBody').val().indexOf("|") >= 0) {
                alert("| is a reserved character");
            }
            else {
                if (isNumeric($('#txtGridOrder').val()) == false) {
                    alert("Order must be a number between 1 and 40");
                }
                else {
                    var columnParameters = $('#hdnGridColumns').val();
                    var gridParameters = $('#ddlNewGridWebHookType').val() + "|" + $('#txtNewGridWebHookURL').val().replace(/'/g,"-S-") + "|" + $('#txtNewGridWebHookBody').val().replace(/'/g,"-S-") + "|" + $('#txtGridOrder').val() + "|" + $('#txtNewGridValueField').val().replace(/'/g,"-S-") + "|" + columnParameters + "|" + editContainer;
                        var elementIndex = -1;
                        $('#' + editContainer + ' > div').each(function () {
                            elementIndex = elementIndex + 1;
                            $(this).find('input').each(function (t) {
                                if (selectedElement == elementIndex) {
                                    $(this).val(gridParameters);
                                }
                   
                            });
                        });
                  $("#divEditGridModal").dialog('close');
                }
            }


        }

        if (editContainer.indexOf("1") >= 0) {
            nodeContent1[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("2") >= 0) {
            nodeContent2[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("3") >= 0) {
            nodeContent3[selectedNode-1] = $('#' + editContainer).html();
        }
    }

        function SaveListBox() {
        if ($('#txtListBoxVariable').val().length == 0 || $('#txtListBoxLabel').val().length == 0 ) {
            alert("Please provide a Variable name to store the value and a Label for the List Box");
        }
        else {
            if ($('#txtListBoxVariable').val(), indexOf("|") >= 0 || $('#txtListBoxLabel').val().indexOf("|" >= 0 || $('#txtNewSelectListWebHookURL').val().indexOf("|") >= 0) || $('#txtNewSelectListWebHoodBody').val().indexOf("|") >= 0 || $('#txtListBoxDisplayWebHook').val().indexOf("|") >= 0 || $('#txtListBoxValueWebHook').val().indexOf("|") >= 0) {
                alert("| is a reserved character");
            }
            else {
                if (isNumeric($('#txtListBoxOrder').val()) == false) {
                    alert("Order must be a number between 1 and 40");
                }
                else {
                    var isfound = false;
                    var i;
                    for (i = 0; i < variables.length; i++) {
                        if (variables[i] == $('#txtListBoxVariable').val()) {
                            isfound = true;
                        }
                    }

                    if (isfound == false) {
                        variables[variables.length] = $('#txtListBoxVariable').val();
                    }
                        variables[variables.length] = $('#txtListBoxVariable').val();

                        if (listBoxEditMode == 0) {
                            var optionValuesString = "";
                            $('#ddlEditListBoxOptions option').each(function () {
                                if (optionValuesString.length > 0) {
                                    optionValuesString = optionValuesString + ",";
                                }
                                optionValuesString = optionValuesString + $(this).text().replace(/'/g,"-S-") + ":" + $(this).val().replace(/'/g,"-S-");
                            });
                    
                            var stringValues = listBoxEditMode + "|" + $('#txtListBoxVariable').val().replace(/'/g,"-S-") + "|" + $('#txtListBoxLabel').val().replace(/'/g,"-S-") + "|" + optionValuesString + "|" + $('#txtListBoxOrder').val() + "|" + editContainer;
                            var elementIndex = -1;
                            $('#' + editContainer + ' > div').each(function () {
                                elementIndex = elementIndex + 1;
                                $(this).find('input').each(function (t) {
                                    if (selectedElement == elementIndex) {
                                        $(this).val(stringValues);
                                    }

                                });
                            });
                        }
                        else {
                            var stringValues = listBoxEditMode + "|" + $('#txtListBoxVariable').val().replace(/'/g,"-S-") + "|" + $('#txtListBoxLabel').val().replace(/'/g,"-S-") + "|"+ $('#ddlNewSelectListWebHookType').val() + "|" + $('#txtNewSelectListWebHookURL').val().replace(/'/g,"-S-") + "|";
                            stringValues = stringValues + $('#txtNewSelectListWebHoodBody').val().replace(/'/g,"-S-")  + "|" + $('#txtListBoxDisplayWebHook').val().replace(/'/g,"-S-") + "|" + $('#txtListBoxValueWebHook').val().replace(/'/g,"-S-") + "|" + $('#txtListBoxOrder').val() + "|" + editContainer;
                            var elementIndex = -1;
                            $('#' + editContainer + ' > div').each(function () {
                                elementIndex = elementIndex + 1;
                                $(this).find('input').each(function (t) {
                                    if (selectedElement == elementIndex) {
                                        $(this).val(stringValues);
                                    }

                                });
                            });
                        }

                         $("#divEditListBoxModal").dialog('close');
                }
            }

        }

        if (editContainer.indexOf("1") >= 0) {
            nodeContent1[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("2") >= 0) {
            nodeContent2[selectedNode-1] = $('#' + editContainer).html();
        }
        if (editContainer.indexOf("3") >= 0) {
            nodeContent3[selectedNode-1] = $('#' + editContainer).html();
        }
    }

    function addListBoxOption() {

        if ($('#txtListBoxDisplayAdd').val() == "" || $('#txtListBoxValueAdd').val() == "") {
            alert("Please add Display Text and Value");

        }
        else {
            if ($('#txtListBoxDisplayAdd').val().indexOf("|") >=0 || $('#txtListBoxValueAdd').val().indexOf("|") >=0) {
                 alert("| is a reserved character");
            }
            else {
                $('#ddlEditListBoxOptions').append(new Option($('#txtListBoxDisplayAdd').val(), $('#txtListBoxValueAdd').val(), true, true));
                $('#txtEditListBoxSelectedValue').val($('#txtListBoxValueAdd').val());
            
                $('#txtListBoxDisplayAdd').val("");
                $('#txtListBoxValueAdd').val("");
            }

        }
    }


    function addGridcolumn() {

        if ($('#txtNewGridHeaderText').val() == "" || $('#txtNewGridHeaderValue').val() == "") {
            alert("Please add Column Header and Value");

        }
        else {
            if ($('#txtNewGridHeaderText').val().indexOf("|") >= 0 || $('#txtNewGridHeaderValue').val().indexOf("|") >= 0) {
                alert("| is a reserved character");
            }
            else {
                var hdnValue = $('#hdnGridColumns').val();
                var displayValue = $('#lblEditGridColumnDisplay').html();

                if (hdnValue.length > 0)
                {
                     hdnValue = hdnValue + ",";
                }
                if (displayValue.length > 0)
                {
                     displayValue = displayValue + "<br />";
                }

                hdnValue = hdnValue + $('#txtNewGridHeaderText').val() + ":" + $('#txtNewGridHeaderValue').val() + ":" + $('#ddlNewGridColumnVisible').val() + ":" + $('#ddlNewGridColumnType').val();
                var visibleDisplay = "";
                if ($('#ddlNewGridColumnVisible').val() == 1)
                {
                    visibleDisplay = "True";
                }
                else
                {
                    visibleDisplay = "False";
                }
                displayValue = displayValue + "Column Header: " + $('#txtNewGridHeaderText').val() + " Value Field: " + $('#txtNewGridHeaderValue').val() + " Visible: " + visibleDisplay + " Type: " + $('#ddlNewGridColumnType').val();
            
                $('#hdnGridColumns').val(hdnValue.replace(/'/g,"-S-"));
                $('#lblEditGridColumnDisplay').html(displayValue);

                $('#txtNewGridHeaderText').val("");
                $('#txtNewGridHeaderValue').val("");
            }

        }
    }


    function ClearGridColumns() {

            $('#hdnGridColumns').val("");
            $('#lblEditGridColumnDisplay').html("");
    }

    function RemoveListBoxOption() {
        if ($('#ddlEditListBoxOptions').val().length > 0) {
            $('#ddlEditListBoxOptions option:selected').remove();
        }
          
    }



    function EditListBoxToggleWebHook(option) {
        if (option == 0) {
            listBoxEditMode = 0;
            $('#EditListBoxManual').show();
            $('#EditListBoxWebHook').hide();
        }
        else {
            listBoxEditMode = 1;
            $('#EditListBoxManual').hide();
            $('#EditListBoxWebHook').show();
        }
    }

    $(document).ready(function () {
        baseAPIURL  = $("#MainContent_txtBaseURL").val();
        EditListBoxToggleWebHook(0);

        $("#txtContentAreaText").jqte(); 

        $('#ddlEditListBoxOptions').on('change', function () {
            $('#txtEditListBoxSelectedValue').val($('#ddlEditListBoxOptions').val());
        });

        $('#ddlNewButtonAction').on('change', function () {
             $('#divNewButtongoTo').hide();
             $('#divNewButtonWebHook').hide();
             if ($('#ddlNewButtonAction').val() == "GoToNode") {
                 $('#divNewButtongoTo').show();
                 $('#divNewButtonWebHook').hide();

                 $('#ddlEditButtonNodes').empty();
                 var i;
              
                 for (i = 0; i < nodes.length; i++) {
                     if (i != (selectedNode-1)) {
                         $('#ddlEditButtonNodes').append(new Option(nodes[i], nodes[i], false, false));
                     }
                 }
                 


             }
                 if ($('#ddlNewButtonAction').val() == "WebHook") {
                 $('#divNewButtongoTo').hide();
                 $('#divNewButtonWebHook').show();
             }
        });

     $('#divNodeToolBar').hide();
     $('#divNodeEdit').hide();


     $("#divEditContentAreaModal").dialog({
        height: 400,
        width: 700,
        resizable: false,
        title: "Edit Content Area"
    });
     $("#divEdittextBoxModal").dialog({
        height: 400,
        width: 500,
        resizable: false,
        title: "Edit Text Box"
    });
     $("#divEditListBoxModal").dialog({
        height: 500,
        width: 800,
        resizable: false,
        title: "Edit List Box"
    });
     $("#divEditButtonModal").dialog({
        height: 600,
        width: 500,
        resizable: false,
        title: "Edit Button"
    });
    $("#divAddNodeModal").dialog({
        height: 300,
        width: 500,
        resizable: false,
        title: "Add Node"
        });
    $("#divAddActionModal").dialog({
        height: 300,
        width: 500,
        resizable: false,
        title: "Add Action"
        });
     $("#divEditActionWebHook").dialog({
        height: 600,
        width: 800,
        resizable: true,
        title: "Edit Action Web Hook"
        });
    $("#divEditActionGoTo").dialog({
        height: 600,
        width: 800,
        resizable: false,
        title: "Edit Action Go To Node"
        });

        $("#divMessageModal").dialog({
        height: 300,
        width: 500,
        resizable: false,
        title: "Message"
        });

        $("#divAddScriptModal").dialog({
        height: 300,
        width: 500,
        resizable: false,
        title: "Create Script Name"
        });

        $("#divEditGridModal").dialog({
        height: 600,
        width: 800,
        resizable: true,
        title: "Edit Grid"
        });

        $("#divBasicContentArea1").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
            drop: function (event, ui) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
                var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divBasicContentArea1 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divBasicContentArea1\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
                $("#divBasicContentArea1").append(contentHTML);
                nodeContent1[selectedNode - 1] = $("#divBasicContentArea1").html();

      }
    });

        $("#divLeftPanelContentArea1").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
          drop: function( event, ui ) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
                var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divLeftPanelContentArea1 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divLeftPanelContentArea1\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
              $("#divLeftPanelContentArea1").append(contentHTML);
        nodeContent1[selectedNode-1] = $("#divLeftPanelContentArea1").html();
      }
        });

        $("#divLeftPanelContentArea2").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
          drop: function( event, ui ) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
              var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divLeftPanelContentArea2 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divLeftPanelContentArea2\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
              $("#divLeftPanelContentArea2").append(contentHTML);
              nodeContent2[selectedNode-1] = $("#divLeftPanelContentArea2").html();
      }
        });

        $("#divLeftPanelContentArea3").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
         drop: function( event, ui ) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
                var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divLeftPanelContentArea3 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divLeftPanelContentArea3\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
             $("#divLeftPanelContentArea3").append(contentHTML);
        nodeContent3[selectedNode-1] = $("#divLeftPanelContentArea3").html();
      }
    });

        $("#divRightPanelContentArea1").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
         drop: function( event, ui ) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
                var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divRightPanelContentArea1 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divRightPanelContentArea1\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
             $("#divRightPanelContentArea1").append(contentHTML);
         nodeContent1[selectedNode-1] = $("#divRightPanelContentArea1").html();
      }
        });

        $("#divRightPanelContentArea2").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
          drop: function( event, ui ) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
                var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divRightPanelContentArea2 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divRightPanelContentArea2\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
              $("#divRightPanelContentArea2").append(contentHTML);
              nodeContent2[selectedNode-1] = $("#divRightPanelContentArea2").html();
      }
        });

        $("#divRightPanelContentArea3").droppable({
          activeClass: 'ui-state-hover',
          hoverClass: 'ui-state-active',
          drop: function( event, ui ) {
                var doppedID = ui.draggable.attr("id");
                var typeAttribute;
                var img = "";
                   var type = "";
                    if (doppedID == "divElementContent") {
                        type = "Content Area";
                        typeAttribute = "ContentArea";
                        img = "img/Icon5.png";
                    }
                    if (doppedID== "divElementTextBox") {
                        type = "Text Box";
                        typeAttribute = "TextBox";
                        img = "img/Icon1.png";
                    }
                    if (doppedID == "divElementListBox") {
                        type = "List Box";
                        typeAttribute = "ListBox";
                        img = "img/Icon2.png";
                    }
                    if (doppedID == "divElementButton") {
                        type = "Button";
                        typeAttribute = "Button";
                        img = "img/Icon4.png";
                    }
                        if (doppedID == "divElementGrid") {
                        type = "Grid";
                        typeAttribute = "Grid";
                        img = "img/Icon3.png";
                    }

        var childrenCount = $('#divRightPanelContentArea3 > div').length;

         var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"divRightPanelContentArea3\")'>" + (childrenCount + 1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='' /></div>";
              $("#divRightPanelContentArea3").append(contentHTML);
              nodeContent3[selectedNode-1] = $("#divRightPanelContentArea3").html();
      }
        });



        $("#divElementContent").draggable({ revert: true });
        $("#divElementTextBox").draggable({ revert: true });
        $("#divElementListBox").draggable({ revert: true });
        $("#divElementButton").draggable({ revert: true });
        $( "#divElementGrid" ).draggable({ revert: true });

     $("#divEditContentAreaModal").dialog('close');
     $("#divEdittextBoxModal").dialog('close');
     $("#divEditListBoxModal").dialog('close');
     $("#divEditButtonModal").dialog('close');
     $("#divAddNodeModal").dialog('close');
     $("#divAddActionModal").dialog('close');
     $("#divEditActionGoTo").dialog('close');
     $("#divEditActionWebHook").dialog('close');
     $("#divMessageModal").dialog('close');
     $("#divAddScriptModal").dialog('close');
     $("#divEditGridModal").dialog('close');

    $("#divNewButtonWebHook").hide();
    $("#divNewButtongoTo").hide();

    $("#divBasicLayout").hide();
    $("#divLeftPanelLayout").hide();
        $("#divRightPanelLayout").hide();

        //$("#divLeftPanelLayout").show();
        //Initialize Loading

        // get existing scripts
        
        loadExistingScripts();



});

    function loadExistingScripts()
    {
                // get existing scripts
      var entityId = $("#MainContent_txtEntityId").val();
      var url = baseAPIURL + 'AdminScript/GetScripts?entityId=' + entityId;
    
        $.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "jsonp",
            url: url,
            headers: {
           "accept": "application/json",
           "Access-Control-Allow-Origin":"*"
             },
            success: function (result) {
                  alert("sucess");
                 $("#ddlExistingScripts").empty();
                 $.each(result, function (key, entry) {
                    $("#ddlExistingScripts").append($('<option></option>').attr('value', entry.ScriptId).text(entry.ScriptName));
                  })
            },
            jsonpCallback: function (result) {
                  alert(result);

             },

            failure: function (errMsg) {
                alert("failed");
            }
        });
    }

    function LoadScript() {
        CancelEditNode();
        if ($("#ddlExistingScripts").val() == null) {
            alert("There is no script selected to load");
        }
        else {
            if ($("#ddlExistingScripts").val().length == 0) {
                alert("There is no script selected to load");
            }
            else {
                $("#lblMessage").html("<h3>Loading Script. Please wait ...</h3>");
                $("#divMessageModal").dialog('open');
                clearScript();
                scriptId = $("#ddlExistingScripts").val();
                loadNodes();
                loadActions();
                loadStartAction();
                $("#divMessageModal").dialog('close');
            }
        }

    }

    function loadStartAction() {
      var entityId = $("#MainContent_txtEntityId").val();
      var url = baseAPIURL + 'AdminStartAction/GetStartAction?scriptId=' + scriptId;
        $.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: url,
            success: function (result) {
                $('#ddlStartAction').val(result);
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
 
    }

    function loadNodes() {
      var entityId = $("#MainContent_txtEntityId").val();
      var url = baseAPIURL + 'AdminNode/GetNodes?scriptId=' + scriptId;
        $.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: url,
            success: function (result) {
                var i = -1;
                $.each(result, function (key, entry) {
                    i = i + 1;
                    addNode(entry.NodeName,entry.NodeTemplateId);
                    loadElements(entry.NodeId,i);
                  })
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }

   function loadActions() {
      var entityId = $("#MainContent_txtEntityId").val();
      var url = baseAPIURL + 'AdminAction/getActions?scriptId=' + scriptId;
        $.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: url,
            success: function (result) {
                $.each(result, function (key, entry) {
                    actions[actions.length] = entry.ActionName;
                    var value = entry.ActionParameters;
                    var typeAttribute = entry.ActionType;

                    var contentHTML = "";
                    contentHTML = "<div id='div_action_" + (actions.length-1) + "' actionName='" + entry.ActionName + "' type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditAction(" + (actions.length-1) + ")'>" + (actions.length) + " " + entry.ActionName + ": " + typeAttribute + "<input type='hidden' value='" + value + "'  /></div>";

                    $('#divActionDesign').append(contentHTML);
                    $('#ddlStartAction').append(new Option(entry.ActionName, entry.ActionName, false, false));
                  })
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    }

    function loadElements(nodeId,nodeIndex) {
        var i;
        var nodeCount = nodes.length;
        var url = baseAPIURL + 'AdminElement/getElements?nodeId=' + nodeId;
        $.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: url,
            success: function (result) {
                $.each(result, function (key, entry) {
                    var typeAttribute = entry.eType;
                    var valueList = entry.ParameterValues.split("|");
                    var containerId = valueList[valueList.length - 1];
                   
                    var type = "";
                    var img = "";
                    if (typeAttribute == "ContentArea") {
                        type = "Content Area";
                        img = "img/Icon5.png";
                    }
                    if (typeAttribute== "TextBox") {
                        type = "Text Box";
                        variables[variables.length] = entry.ParameterValues;
                        img = "img/Icon1.png";
                    }
                    if (typeAttribute == "ListBox") {
                        type = "List Box";
                        
                        variables[variables.length] = valueList[1];
                        img = "img/Icon2.png";
                    }
                    if (typeAttribute == "Button") {
                        type = "Button";
                        img = "img/Icon4.png";
                    }
                    if (typeAttribute == "Grid") {
                        type = "Grid";
                        img = "img/Icon3.png";
                    }

                    var childrenCount = 0;


                    if (containerId.indexOf("1") >= 0) {
                        $('#' + containerId).html(nodeContent1[nodeIndex]);
                        childrenCount = $('#' + containerId + ' > div').length;
                        var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"" + containerId + "\")'>" + (childrenCount+1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='" + entry.ParameterValues.replace("'","&#39;") + "' /></div>";

                        nodeContent1[nodeIndex] = nodeContent1[nodeIndex] + contentHTML;
         
                    }
                    if (containerId.indexOf("2") >= 0) {
                        $('#' + containerId).html(nodeContent2[nodeIndex]);
                        childrenCount = $('#' + containerId + ' > div').length;
                        var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"" + containerId + "\")'>" + (childrenCount+1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='" + entry.ParameterValues.replace("'","&#39;") + "' /></div>";

                        nodeContent2[nodeIndex] = nodeContent2[nodeIndex] + contentHTML;
                    }
                    if (containerId.indexOf("3") >= 0) {
                        
                        $('#' + containerId).html(nodeContent3[nodeIndex]);
                        childrenCount = $('#' + containerId + ' > div').length;
                        var contentHTML = "<div type='" + typeAttribute + "' style='border:1px solid black; width=200px;padding-left:15px;background-color:#000000;color:#ffffff' onclick='EditElement(" + childrenCount + ",\"" + containerId + "\")'>" + (childrenCount+1) + " " + "<img src=\"" + img + "\" alt=\"" + type + "\" width =\"200\" height=\"40\" />" + "<input type='hidden' value='" + entry.ParameterValues.replace("'","&#39;") + "' /></div>";

                        nodeContent3[nodeIndex] = nodeContent3[nodeIndex] + contentHTML;
                    }
                })
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });

    }

    function isNumeric(input) {
        var res = false;
        if (input == "1") {
            res = true;
        }
        if (input == "2") {
            res = true;
        }
        if (input == "3") {
            res = true;
        }
        if (input == "4") {
            res = true;
        }
        if (input == "5") {
            res = true;
        }
        if (input == "6") {
            res = true;
        }
        if (input == "7") {
            res = true;
        }
        if (input == "8") {
            res = true;
        }
        if (input == "9") {
            res = true;
        }
        if (input == "10") {
            res = true;
        }
        if (input == "11") {
            res = true;
        }
        if (input == "12") {
            res = true;
        }
        if (input == "13") {
            res = true;
        }
        if (input == "14") {
            res = true;
        }
        if (input == "15") {
            res = true;
        }
        if (input == "16") {
            res = true;
        }
        if (input == "17") {
            res = true;
        }
        if (input == "18") {
            res = true;
        }
        if (input == "19") {
            res = true;
        }
        if (input == "20") {
            res = true;
        }
        if (input == "21") {
            res = true;
        }
        if (input == "22") {
            res = true;
        }
        if (input == "23") {
            res = true;
        }
        if (input == "24") {
            res = true;
        }
        if (input == "25") {
            res = true;
        }
        if (input == "26") {
            res = true;
        }
        if (input == "27") {
            res = true;
        }
        if (input == "28") {
            res = true;
        }
        if (input == "29") {
            res = true;
        }
        if (input == "30") {
            res = true;
        }
        if (input == "31") {
            res = true;
        }
        if (input == "32") {
            res = true;
        }
        if (input == "33") {
            res = true;
        }
        if (input == "34") {
            res = true;
        }
        if (input == "35") {
            res = true;
        }
        if (input == "36") {
            res = true;
        }
        if (input == "37") {
            res = true;
        }
        if (input == "38") {
            res = true;
        }
        if (input == "39") {
            res = true;
        }
        if (input == "40") {
            res = true;
        }
        return res;
    }

</script>

    <asp:HiddenField ID ="txtEntityId" runat="server" />
    <asp:HiddenField ID ="txtBaseURL" runat="server" />
</asp:Content>


