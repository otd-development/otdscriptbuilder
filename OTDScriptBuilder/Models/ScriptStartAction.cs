//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OTDScriptBuilder.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ScriptStartAction
    {
        public long ScriptStartActionId { get; set; }
        public long ScriptId { get; set; }
        public long ActionId { get; set; }
    
        public virtual Action Action { get; set; }
        public virtual Script Script { get; set; }
    }
}
