﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Controls.aspx.cs" Inherits="OTDScriptBuilder.Controls" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="background-color:aquamarine">
    <form id="form1" runat="server">
        <div>
            <br />
            <br />
            <br />
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; First Name: <asp:TextBox ID="txttest" runat="server"></asp:TextBox>
            <br />
            <br />
            <br />
            <br />
            <br />

           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btntest" Text="Continue" runat="server" />
            <br />
            <br />
            <br />
            <br />
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Please Select: 
            <asp:DropDownList ID="ddltest" runat="server">
                <asp:ListItem Text="Option 1"></asp:ListItem>
                <asp:ListItem Text="Option 2"></asp:ListItem>
                <asp:ListItem Text="Option 3"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <br />
            <br />
            <br />
            <div style="width:400px">
                <asp:GridView ID="gvtest" runat="server" AlternatingRowStyle-BackColor="#ccffff" AutoGenerateColumns="false" BackColor="Wheat" HeaderStyle-BackColor="Black" HeaderStyle-ForeColor="White" HorizontalAlign="Right">
                    <Columns>
                        <asp:BoundField HeaderText="First Name" DataField="fn" />
                        <asp:BoundField HeaderText="Last Name" DataField="ln" />
                    </Columns>
                
                </asp:GridView>

            </div>

        </div>
    </form>
</body>
</html>
