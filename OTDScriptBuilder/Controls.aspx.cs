﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OTDScriptBuilder
{
    public partial class Controls : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("fn"));
            dt.Columns.Add(new DataColumn("ln"));
            List<string> row1 = new List<string>();
            row1.Add("Bob");
            row1.Add("Green");
            List<string> row2 = new List<string>();
            row2.Add("Sally");
            row2.Add("West");
            dt.Rows.Add(row1.ToArray());
            dt.Rows.Add(row2.ToArray());
            gvtest.DataSource = dt;
            gvtest.DataBind();
        }
    }
}