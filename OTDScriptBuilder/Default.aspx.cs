﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OTDScriptBuilder
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (txtEntityId.Value == string.Empty)
            {
                if (Session["EntityId"] == null)
                {
                    if (Request.QueryString["EntityId"] == null)
                    {
                        Response.Redirect("AuthError");
                    }
                    else
                    {
                        Session["EntityId"] = int.Parse(Request.QueryString["EntityId"]);
                        Response.Redirect(Request.Path);
                    }
                }
                else
                {
                    txtEntityId.Value = Session["EntityId"].ToString();
                    txtBaseURL.Value = System.Configuration.ConfigurationManager.AppSettings["baseAPIURL"];
                }


            }
        }
    }
}