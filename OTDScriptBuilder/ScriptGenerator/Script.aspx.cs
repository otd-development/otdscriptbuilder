﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OTDScriptBuilder.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OTDScriptBuilder.ScriptGenerator
{
    public partial class Script : System.Web.UI.Page
    {
       
        protected  void Page_Load(object sender, EventArgs e)
        {
            if (Session["scriptId"] == null)
            {
                if (Request.QueryString["scriptId"] == null)
                {
                    Response.Redirect("../AuthError");
                }

                foreach (var q in Request.QueryString.AllKeys)
                {
                    Session[q] = Request.QueryString[q];
                }

                Response.Redirect(Request.Url.LocalPath,true);
            }
            else
            {
                // Get Start Action
  
                    long id = long.Parse((string)Session["scriptId"]);
                    using (OTDScriptsEntities db = new OTDScriptsEntities())
                    {
                        var startAction = db.ScriptStartActions.Where(w => w.ScriptId == id).Select(s=>s.Action).FirstOrDefault();
                        if (startAction == null)
                        {
                            lblMess.Text = "Invalid script: missing Start Action!";

                        }
                        else
                        {
                            lblMess.Text = string.Empty;
                            executeAction(startAction.ActionName, startAction.ActionTypeId, startAction.ActionParameters);
                        }
                    }
            

            }

         
        }

        protected void handleAction(PlaceHolder ph)
        {
            foreach (var c in ph.Controls)
            {
                string varName = string.Empty;
                object varValue = null;

                if (c is TextBox)
                {
                    varName = ((TextBox)c).ID.Replace("txt", "");
                    varValue = ((TextBox)c).Text;
                    Session[varName] = varValue;
                }
                if (c is DropDownList)
                {
                    varName = ((DropDownList)c).ID.Replace("ddl", "");
                    varValue = ((DropDownList)c).SelectedValue;
                    Session[varName] = varValue;
                }
                if (c is GridView)
                {

                    var GVName = ((GridView)c).ID.Replace("gv", "");
                    var selectedIndex = ((GridView)c).SelectedIndex;
                    if (selectedIndex >= 0)
                    {
                        DataTable dtGV = (DataTable)Session[GVName];
                        var row = dtGV.Rows[selectedIndex].ItemArray;

                        string[] columnNames = (from dc in dtGV.Columns.Cast<DataColumn>()
                                                select dc.ColumnName).ToArray();

                        for (int i = 0; i < columnNames.Length; i++)
                        {
                            varName = columnNames[1];
                            varValue = row[1];
                            Session[varName] = varValue;
                        }
                    }


                }


            }
        }

        protected void ActionButton_Click(object sender, EventArgs e)
        {
            if (pnl1.Visible)
            {
                handleAction(phContent1);
            }

            if (pnl2.Visible)
            {
                handleAction(phContent2);
                handleAction(phContent3);
                handleAction(phContent4);
            }

            if (pnl3.Visible)
            {
                handleAction(phContent5);
                handleAction(phContent6);
                handleAction(phContent7);
            }
            string actionName = ((Button)sender).ID.Replace("btn","");
            executeAction(actionName);
        }

        protected void executeAction(string actionName)
        {
            long id = long.Parse((string)Session["scriptId"]);

            using (OTDScriptsEntities db = new OTDScriptsEntities())
            {
                var actionRecord = db.Actions.Where(a => a.ActionName == actionName && a.ScriptId == id).FirstOrDefault();
                if (actionRecord == null)
                {
                    lblMess.Text = "Invalid action: " + actionName;
                }
                else
                {
                    lblMess.Text = string.Empty;
                    int actionType = actionRecord.ActionTypeId;
                    string actionParameters = actionRecord.ActionParameters;
                    executeAction(actionName, actionType, actionParameters);
                }
            }

       }

        protected  void executeAction(string actionName, int actionType, string actionParameters)
        {
            lblMess.Text = string.Empty;
            long id = long.Parse((string)Session["scriptId"]);
            if (actionType == 1)
            {
                // GoTo
                //Clear Controls
                string nodeName = string.Empty;

                // get correct NodeName
                actionParameters = actionParameters.Replace("-S-","'");
                foreach (var k in Session.Keys)
                {
                    string tag = "%" + k.ToString() + "%";
                    actionParameters = actionParameters.Replace(tag, Session[k.ToString()].ToString());
                }

                var paramslist = actionParameters.Split('|');
                
                foreach (var prow in paramslist)
                {
                    if (nodeName == string.Empty)
                    {
                        if (prow.Length > 0)
                        {
                            var r = prow.Split(':');
                            try
                            {
                                switch(r[3])
                                {
                                    case "int":
                                        var option1 = int.Parse(r[0]);
                                        var option2 = int.Parse(r[2]);
                                        if (r[1] == "equal")
                                        {
                                            if (option1 == option2)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "notequal")
                                        {
                                            if (option1 != option2)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "greater")
                                        {
                                            if (option1 > option2)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "less")
                                        {
                                            if (option1 < option2)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        break;
                                    case "long":
                                        var option3 = long.Parse(r[0]);
                                        var option4 = long.Parse(r[2]);
                                        if (r[1] == "equal")
                                        {
                                            if (option3 == option4)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "notequal")
                                        {
                                            if (option3 != option4)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "greater")
                                        {
                                            if (option3 > option4)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "less")
                                        {
                                            if (option3 < option4)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        break;
                                    case "decimal":
                                        var option5 = decimal.Parse(r[0]);
                                        var option6 = decimal.Parse(r[2]);
                                        if (r[1] == "equal")
                                        {
                                            if (option5 == option6)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "notequal")
                                        {
                                            if (option5 != option6)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "greater")
                                        {
                                            if (option5 > option6)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "less")
                                        {
                                            if (option5 < option6)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        break;
                                    case "DateTime":
                                        var option7 = DateTime.Parse(r[0]);
                                        var option8 = DateTime.Parse(r[2]);
                                        if (r[1] == "equal")
                                        {
                                            if (option7 == option8)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "notequal")
                                        {
                                            if (option7 != option8)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "greater")
                                        {
                                            if (option7 > option8)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "less")
                                        {
                                            if (option7 < option8)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        break;
                                    case "string":
                                        var option9 = r[0];
                                        var option10 = r[2];
                                        if (r[1] == "equal")
                                        {
                                            if (option9 == option10)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "notequal")
                                        {
                                            if (option9 != option10)
                                            {
                                                nodeName = r[4];
                                            }
                                        }
                                        if (r[1] == "greater")
                                        {
                                            lblMess.Text = "Greater Than comparrison is not valid with Type String";
                                        }
                                        if (r[1] == "less")
                                        {
                                            lblMess.Text = "Less Than comparrison is not valid with Type String";
                                        }
                                        break;
                                }
                            }
                            catch
                            {
                                lblMess.Text = "Error in Action Condition: Conversion to Type is not valid";
                            }

 
                        }
                        
                    }
                }



                phContent1.Controls.Clear();
                phContent2.Controls.Clear();
                phContent3.Controls.Clear();
                phContent4.Controls.Clear();
                phContent5.Controls.Clear();
                phContent6.Controls.Clear();
                phContent7.Controls.Clear();

                if (nodeName != string.Empty)
                {
                    long nodeId = 0;
                    using (OTDScriptsEntities db = new OTDScriptsEntities())
                    {
                        
                        var n = db.Nodes.Where(w => w.ScriptId == id && w.NodeName == nodeName).First();
                        int templateId = n.NodeTemplateId.Value;

                        switch(templateId)
                        {
                            case 1:
                                pnl1.Visible = true;
                                pnl2.Visible = false;
                                pnl3.Visible = false;
                                break;
                            case 2:
                                pnl1.Visible = false;
                                pnl2.Visible = true;
                                pnl3.Visible = false;
                                break;
                            case 3:
                                pnl1.Visible = false;
                                pnl2.Visible = false;
                                pnl3.Visible = true;
                                break;
                        }

                        nodeId = n.NodeId;
                        var nodeElements = db.Elements.Where(w => w.NodeId == nodeId).OrderBy(o=>o.ElementOrder).ToList();
                        foreach (var e in nodeElements)
                        {
                            Literal litEndBar = new Literal();
                            litEndBar.Text = "<br /><br />";
                            litEndBar.ID = "lit_node" + nodeId.ToString() + "_element" + e.ElementId + "end";

                            var elementParameters = db.ElementParameters.Where(w => w.ElementId == e.ElementId).First().ParameterValues;
                            var values = elementParameters.Replace("-S-","'").Split('|');
                            string contentArea = values[values.Length - 1];

                            if (e.ElementTypeId == 1)
                            {
                                Literal l = new Literal();
                                l.ID = "lit_node" + nodeId.ToString() + "_element" + e.ElementId;
                           
                                

                                string elementText = values[1];

                                foreach (var k in Session.Keys)
                                {
                                    string tag = "%" + k.ToString() + "%";
                                    elementText = elementText.Replace(tag, Session[k.ToString()].ToString());
                                }
                                l.Text = elementText;
                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(l);
                                        break;
                                }
                               
                            }
                            if (e.ElementTypeId == 2)
                            {
                                string lblText = values[1];
                                foreach (var k in Session.Keys)
                                {
                                    string tag = "%" + k.ToString() + "%";
                                    lblText = lblText.Replace(tag, Session[k.ToString()].ToString());
                                }

                                Literal l = new Literal();
                                l.ID = "lit_node" + nodeId.ToString() + "_element" + e.ElementId + "_lbl";
                                l.Text = "<span>" + lblText + ": </span>";
                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(l);
                                        break;
                                }

                                TextBox t = new TextBox();
                                t.ID = "txt" + values[0];
                                if (Session[values[0]] != null)
                                {
                                    t.Text = (string)Session[values[0]];
                                }

                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(t);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(t);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(t);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(t);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(t);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(t);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(t);
                                        break;
                                }
                            }
                            if (e.ElementTypeId == 4)
                            {
                                Button btn = new Button();
                                btn.ID = "btn" + values[1];
                                btn.Text = values[0];
                                btn.Click += ActionButton_Click;

                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(btn);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(btn);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(btn);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(btn);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(btn);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(btn);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(btn);
                                        break;
                                }
                            }
                            if (e.ElementTypeId == 3)
                            {

                                string lblText = values[2];
                                foreach (var k in Session.Keys)
                                {
                                    string tag = "%" + k.ToString() + "%";
                                    lblText = lblText.Replace(tag, Session[k.ToString()].ToString());
                                }

                                Literal l = new Literal();
                                l.ID = "lit_node" + nodeId.ToString() + "_element" + e.ElementId + "_lbl";
                                l.Text = "<span>" + lblText + ": </span>";
                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(l);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(l);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(l);
                                        break;
                                }

                                DropDownList ddl = new DropDownList();
                                ddl.ID = "ddl" + values[1];
                            
                                if (values[0] == "0")
                                {
                                    var listOptions = values[3].Split(',');
                                    for (var i = 0; i < listOptions.Length; i++)
                                    {
                                        var li = listOptions[i].Split(':');
                                        ddl.Items.Add(new ListItem(li[0], li[1]));
                                    }
                                }
                                else
                                {
                                    // webkook
                                    using (HttpClient client = new HttpClient())
                                    {
                                        string jsonBody = values[5];
                                        string apiURL = values[4];

                                        foreach (var k in Session.Keys)
                                        {
                                            string tag = "%" + k.ToString() + "%";
                                            jsonBody = jsonBody.Replace(tag,Session[k.ToString()].ToString());
                                            apiURL = apiURL.Replace(tag, Session[k.ToString()].ToString());
                                        }

                                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                        string jsonString1 = string.Empty;
                                        if (values[3] == "POST")
                                        {
                                            var task1 = client.PostAsync(apiURL, new StringContent(jsonBody, Encoding.UTF8, "application/json")).Result;
                                            jsonString1 = task1.Content.ReadAsStringAsync().Result;
                                        }
                                        if (values[3] == "GET")
                                        {
                                            var task1 = client.GetAsync(apiURL).Result;
                                            jsonString1 = task1.Content.ReadAsStringAsync().Result;
                                        }
                                        if (values[3] == "PUT")
                                        {
                                            var task1 = client.PutAsync(apiURL, new StringContent(jsonBody, Encoding.UTF8, "application/json")).Result;
                                            jsonString1 = task1.Content.ReadAsStringAsync().Result;
                                        }




                                        var obj = JToken.Parse(jsonString1);
                                        //obj = JsonConvert.DeserializeObject(jsonString1);

                                  

                                            foreach (var item in obj.Children())
                                            {
                                                var itemProperties = item.Children<JProperty>();
                                                var myFieldInfoDisplay = itemProperties.FirstOrDefault(x => x.Name.ToUpper() == values[6].ToUpper());
                                                var displayValue = myFieldInfoDisplay.Value; ////This is a JValue type

                                            var myFieldInfoValue = itemProperties.FirstOrDefault(x => x.Name.ToUpper() == values[7].ToUpper());
                                            var valueValue = myFieldInfoValue.Value; ////This is a JValue type

                                            ddl.Items.Add(new ListItem { Text = displayValue.ToString(), Value = valueValue.ToString() });


                                        }


                                    }
                                }

                                if (Session[values[1]] != null)
                                {
                                    ddl.SelectedValue = Session[values[1]].ToString();
                                }
                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(ddl);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(ddl);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(ddl);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(ddl);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(ddl);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(ddl);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(ddl);
                                        break;
                                }
                            }

                            if (e.ElementTypeId == 5)
                            {
                                string jsonBody = values[2];
                                string apiURL = values[1];
                                string definedvalueField = values[4];
                                string gridID =  "grid_node" + nodeId.ToString() + "_element" + e.ElementId + "";

                                string gridvalueField = gridID + "_ValueField";
                                Session[gridvalueField] = definedvalueField;
                                foreach (var k in Session.Keys)
                                {
                                    string tag = "%" + k.ToString() + "%";
                                    jsonBody = jsonBody.Replace(tag, Session[k.ToString()].ToString());
                                    apiURL = apiURL.Replace(tag, Session[k.ToString()].ToString());
                                }

                                DataTable dtGrid = new DataTable();
                                using (HttpClient client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                    string jsonString1 = string.Empty;
                                    if (values[0] == "POST")
                                    {
                                        var task1 = client.PostAsync(apiURL, new StringContent(jsonBody, Encoding.UTF8, "application/json")).Result;
                                        jsonString1 = task1.Content.ReadAsStringAsync().Result;
                                    }
                                    if (values[0] == "GET")
                                    {
                                        var task1 = client.GetAsync(apiURL).Result;
                                        jsonString1 = task1.Content.ReadAsStringAsync().Result;
                                    }
                                    if (values[0] == "PUT")
                                    {
                                        var task1 = client.PutAsync(apiURL, new StringContent(jsonBody, Encoding.UTF8, "application/json")).Result;
                                        jsonString1 = task1.Content.ReadAsStringAsync().Result;
                                    }

                                    var obj = JToken.Parse(jsonString1);

                                  
                                    var GridcolList = values[5].Split(',');
                                    for (int i = 0; i < GridcolList.Length;i++)
                                    {
                                        var columnInfo = GridcolList[i].Split(':');
                                        bool isVisible = (columnInfo[2] == "1");
                                        string colheader = columnInfo[0];
                                        string valueField = columnInfo[1];
                                        string fieldType = columnInfo[3];

                                        DataColumn dc = new DataColumn();
                                        dc.ColumnName = valueField;
                                        dc.DataType = System.Type.GetType(fieldType);
   
                                        dtGrid.Columns.Add(dc);
                      

                                    }


                                    foreach (var item in obj.Children())
                                    {
                                        var itemProperties = item.Children<JProperty>();
                                        List<object> rowList = new List<object>();
                                        for (int i = 0; i < GridcolList.Length; i++)
                                        {
                                            var columnInfo = GridcolList[i].Split(':');

                                            string colheader = columnInfo[0];
                                            string valueField = columnInfo[1];
                                            string fieldType = columnInfo[3];

                                            var myFieldInfoDisplay = itemProperties.FirstOrDefault(x => x.Name == valueField);
                                            var displayValue = myFieldInfoDisplay.Value; ////This is a JValue type

                                            rowList.Add(displayValue);

                                        }


                                        dtGrid.Rows.Add(rowList.ToArray());

                                    
                                      
                                    }

                                    
                                }

                                Session[gridID] = dtGrid;

                                GridView gv = new GridView();
                                gv.ID = gridID;
                                gv.SelectedIndexChanged += Gv_SelectedIndexChanged;
                                var colList = values[5].Split(',');
                                for (int i = 0; i < colList.Length; i++)
                                {
                                    var columnInfo = colList[i].Split(':');
                                    bool isVisible = (columnInfo[2] == "1");
                                    string colheader = columnInfo[0];
                                    string valueField = columnInfo[1];
                                    BoundField bf = new BoundField();
                                    bf.HeaderText = colheader;
                                    bf.DataField = valueField;
                                    if (!isVisible)
                                    {
                                        bf.Visible = false;
                                    }

                                    
                                    gv.Columns.Add(bf);

                                }
                                gv.AutoGenerateSelectButton = true;
                                gv.AutoGenerateColumns = false;
                                gv.AlternatingRowStyle.BackColor = System.Drawing.Color.LightGray;
                                gv.SelectedRowStyle.BackColor = System.Drawing.Color.Black;
                                gv.SelectedRowStyle.ForeColor = System.Drawing.Color.White;
                                gv.DataSource = dtGrid;
                                gv.DataBind();

                                if (Session[definedvalueField] != null)
                                {
                                    int selectedindex = -1;
                                    foreach(DataRow row in dtGrid.Rows)
                                    {
                                        selectedindex++;

                                        try
                                        {
                                            if (row[definedvalueField].ToString() == Session[definedvalueField].ToString())
                                            {
                                                gv.SelectedIndex = selectedindex;
                                            }
                                        }
                                        catch
                                        {
                                            // Do nothing.... If the idiots defined a value field that was NOT included in the Column definition
                                            // What do they expect!?!
                                        }

                                         

                                    }
                                }



                                switch (contentArea)
                                {
                                    case "divBasicContentArea1":
                                        phContent1.Controls.Add(gv);
                                        break;
                                    case "divLeftPanelContentArea1":
                                        phContent2.Controls.Add(gv);
                                        break;
                                    case "divLeftPanelContentArea2":
                                        phContent3.Controls.Add(gv);
                                        break;
                                    case "divLeftPanelContentArea3":
                                        phContent4.Controls.Add(gv);
                                        break;
                                    case "divRightPanelContentArea1":
                                        phContent5.Controls.Add(gv);
                                        break;
                                    case "divRightPanelContentArea2":
                                        phContent6.Controls.Add(gv);
                                        break;
                                    case "divRightPanelContentArea3":
                                        phContent7.Controls.Add(gv);
                                        break;
                                }
                            }

                            switch (contentArea)
                            {
                                case "divBasicContentArea1":
                                    phContent1.Controls.Add(litEndBar);
                                    break;
                                case "divLeftPanelContentArea1":
                                    phContent2.Controls.Add(litEndBar);
                                    break;
                                case "divLeftPanelContentArea2":
                                    phContent3.Controls.Add(litEndBar);
                                    break;
                                case "divLeftPanelContentArea3":
                                    phContent4.Controls.Add(litEndBar);
                                    break;
                                case "divRightPanelContentArea1":
                                    phContent5.Controls.Add(litEndBar);
                                    break;
                                case "divRightPanelContentArea2":
                                    phContent6.Controls.Add(litEndBar);
                                    break;
                                case "divRightPanelContentArea3":
                                    phContent7.Controls.Add(litEndBar);
                                    break;
                            }
                        }
                    }
                }


            }
            else
            {
                // WebHook
                using (HttpClient client = new HttpClient())
                {
                    var values = actionParameters.Split('|');
                    string jsonBody = values[2];
                    string apiURL = values[1];

                    foreach (var k in Session.Keys)
                    {
                        string tag = "%" + k.ToString() + "%";
                        jsonBody = jsonBody.Replace(tag, Session[k.ToString()].ToString());
                        apiURL = apiURL.Replace(tag, Session[k.ToString()].ToString());
                    }

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string jsonString1 = string.Empty;
                    if (values[0] == "POST")
                    {
                        var task1 = client.PostAsync(apiURL, new StringContent(jsonBody, Encoding.UTF8, "application/json")).Result;
                        jsonString1 = task1.Content.ReadAsStringAsync().Result;
                    }
                    if (values[0] == "GET")
                    {
                        var task1 = client.GetAsync(apiURL).Result;
                        jsonString1 = task1.Content.ReadAsStringAsync().Result;
                    }
                    if (values[0] == "PUT")
                    {
                        var task1 = client.PutAsync(apiURL, new StringContent(jsonBody, Encoding.UTF8, "application/json")).Result;
                        jsonString1 = task1.Content.ReadAsStringAsync().Result;
                    }



                    try
                    {
                        var obj = JToken.Parse(jsonString1);
                        var itemProperties = obj.Children<JProperty>();

                        foreach (var p in itemProperties)
                        {
                            Session[p.Name] = p.Value;
                        }


                    }
                    catch(Exception ex)
                    {
                        //lblMess.Text = "Error parsing Web Hook Response. Response should be a simple single level object";
                    }


                    string nextAction = values[4];
                    executeAction(nextAction);
                }
            }

        }

        private void Gv_SelectedIndexChanged(object sender, EventArgs e)
        {
            string valuefield = ((GridView)sender).ID + "_ValueField";
            if (Session[valuefield] != null)
            {
                string valueColumn = Session[valuefield].ToString();

                DataTable dt = (DataTable)((GridView)sender).DataSource;
                object value = dt.Rows[((GridView)sender).SelectedIndex].Field<object>(valueColumn);

                Session[Session[valuefield].ToString()] = value;
                
            }
        }
    }


}