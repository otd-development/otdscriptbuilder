﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Script.aspx.cs" Inherits="OTDScriptBuilder.ScriptGenerator.Script" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" style="height:100%;width:100%">
<head runat="server">
    <title></title>
</head>
<body style="background-color:aliceblue;height:100%;width:100%">
    <form id="form1" runat="server" style="height:100%;width:100%">
        <div style="height:100%;width:100%">
            <asp:Label ID="lblMess" runat="server" Font-Size="Large" Font-Bold="true" ForeColor="Maroon"></asp:Label>
            <hr />

            <asp:Panel ID="pnl1" runat="server" Visible="false" Height="100%">
             <div id="divBasicLayout"  style="width:100%; height:100%">
                            <table style="width:100%; height:100%">
                                <tr>
                                    <td style="padding:5px 10px 5px 10px; vertical-align:top">
                                    <asp:PlaceHolder ID="phContent1" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                            </table>
             </div>

            </asp:Panel>
            <asp:Panel ID="pnl2" runat="server" Visible="false" Height="100%">
                        <div id="divLeftPanelLayout"  style="width:100%; height:100%">
                            <table style="width:100%; height:100%" border="0">
                                <tr>
                                    <td style="vertical-align:top; text-align:left; width:30%; height:100%; padding:5px 10px 5px 10px" rowspan="2">
                                        <asp:PlaceHolder ID="phContent2" runat="server"></asp:PlaceHolder>

                                    </td>
                                   <td style="vertical-align:top; text-align:left;width: 100%; height:100%;padding:5px 10px 5px 10px">
                                        <asp:PlaceHolder ID="phContent3" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr>
 
                                    <td style="vertical-align:top; text-align:left;width: 30%; height:100%;padding:5px 10px 5px 10px">
                                        <asp:PlaceHolder ID="phContent4" runat="server"></asp:PlaceHolder>
                                    </td>
                            </tr>
               
                            </table>
                        </div>
            </asp:Panel>
            <asp:Panel ID="pnl3" runat="server" Visible="false" Height="100%">
                        <div id="divRightPanelLayout"  style="width:100%; height:100%">
                            <table style="vertical-align:top; text-align:left;width:100%; height:100%" border="0">
                                <tr>
                                    <td style="vertical-align:top; text-align:center; width:70%;height:100%;padding:5px 10px 5px 10px">
                                        <asp:PlaceHolder ID="phContent5" runat="server"></asp:PlaceHolder>

                                    </td>
                                   <td style="vertical-align:top; text-align:left;width: 30%; height:100%;padding:5px 10px 5px 10px" rowspan="2">
                                        <asp:PlaceHolder ID="phContent6" runat="server"></asp:PlaceHolder>
                                    </td>
                                </tr>
                                <tr>
 
                                    <td style="vertical-align:top; text-align:left;height:100%;padding:5px 10px 5px 10px">
                                        <asp:PlaceHolder ID="phContent7" runat="server"></asp:PlaceHolder>
                                    </td>

                            </tr>
               
                            </table>
                        </div>
            </asp:Panel>


        </div>
    </form>
</body>
</html>
