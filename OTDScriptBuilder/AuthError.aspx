﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuthError.aspx.cs" Inherits="OTDScriptBuilder.AuthError" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 style="font-weight:bold; color:maroon">You have found this page in error. Please close window now.</h1>
</asp:Content>
