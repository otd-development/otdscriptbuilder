//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OTDScriptBuilderAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Action
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Action()
        {
            this.ScriptStartActions = new HashSet<ScriptStartAction>();
        }
    
        public long ActionId { get; set; }
        public long ScriptId { get; set; }
        public string ActionName { get; set; }
        public string ActionParameters { get; set; }
        public int ActionTypeId { get; set; }
    
        public virtual Script Script { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ScriptStartAction> ScriptStartActions { get; set; }
        public virtual ActionType ActionType { get; set; }
    }
}
