﻿using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class OTDScriptValuesController : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetGridMockData()
        {
            List<MockGridData> res = new List<MockGridData>();
            res.Add(new MockGridData { FirstName = "Mickey", LastName = "Mouse", UserID = 1 });
            res.Add(new MockGridData { FirstName = "Donald", LastName = "Duck", UserID = 2 });
            res.Add(new MockGridData { FirstName = "Road", LastName = "Runner", UserID = 3 });
            res.Add(new MockGridData { FirstName = "Tweatie", LastName = "Bird", UserID = 4 });

            return Ok(res);

        }

        [HttpGet]
        public IHttpActionResult GetPrograms(long siteId)
        {
            List<ProgramList> res = new List<ProgramList>();
            switch(siteId)
            {
                case 2:
                    res.Add(new ProgramList { ProgramName= "Windstream", ProgramId = 2 });
                    res.Add(new ProgramList { ProgramName = "Parmalat", ProgramId = 3 });
                    res.Add(new ProgramList { ProgramName = "Best Buy", ProgramId = 6 });
                    break;
                case 20:
                    res.Add(new ProgramList { ProgramName = "AT&T B2B", ProgramId = 1 });
                    res.Add(new ProgramList { ProgramName = "AT&T AMO", ProgramId = 2 });
                    res.Add(new ProgramList { ProgramName = "AT&T Channel", ProgramId = 3 });
                    res.Add(new ProgramList { ProgramName = "Enablement", ProgramId = 4 });
                    res.Add(new ProgramList { ProgramName = "AT&T SLED", ProgramId = 5 });
                    res.Add(new ProgramList { ProgramName = "AT&T LED", ProgramId = 6 });
                    break;
                case 22:
                    res.Add(new ProgramList { ProgramName = "VZW Telesales", ProgramId = 1 });
                    res.Add(new ProgramList { ProgramName = "Best Buy", ProgramId = 2 });
                    res.Add(new ProgramList { ProgramName = "Windstream", ProgramId = 3 });
                    res.Add(new ProgramList { ProgramName = "Shared", ProgramId = 4 });
                    res.Add(new ProgramList { ProgramName = "Zelis", ProgramId = 5 });
                    res.Add(new ProgramList { ProgramName = "Selective Insurance", ProgramId = 6 });
                    res.Add(new ProgramList { ProgramName = "KIA Group", ProgramId = 7 });
                    break;
                case 21:
                    res.Add(new ProgramList { ProgramName = "T-Mobile Telesales", ProgramId = 1 });
                    res.Add(new ProgramList { ProgramName = "T-Mobile Layer 3", ProgramId = 2 });
                    res.Add(new ProgramList { ProgramName = "T-Mobile VIP", ProgramId = 3 });
                    res.Add(new ProgramList { ProgramName = "T-Mobile NCU", ProgramId = 4 });
                    break;
                case 26:
                    res.Add(new ProgramList { ProgramName = "TZ Insurance", ProgramId = 1 });
                    break;
                case 24:
                    res.Add(new ProgramList { ProgramName = "Shared", ProgramId = 1 });
                    res.Add(new ProgramList { ProgramName = "Best Buy", ProgramId = 2 });
                    res.Add(new ProgramList { ProgramName = "Zelis", ProgramId = 3 });
                    res.Add(new ProgramList { ProgramName = "KIA Group", ProgramId = 4 });
                    break;
                case 35:
                    res.Add(new ProgramList { ProgramName = "Windstream", ProgramId = 1 });
                    res.Add(new ProgramList { ProgramName = "Best Buy", ProgramId = 2 });
                    break;
                case 19:
                    res.Add(new ProgramList { ProgramName = "AT&T Mobility", ProgramId = 1 });
                    res.Add(new ProgramList { ProgramName = "Selective Insurance", ProgramId = 2 });
                    break;
            }

            return Ok(res);

        }
    }

    public class ProgramList
    {
        public string ProgramName { get; set; }
        public int ProgramId { get; set; }

    }

    public class MockGridData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public long UserID { get; set; }

    }
}
