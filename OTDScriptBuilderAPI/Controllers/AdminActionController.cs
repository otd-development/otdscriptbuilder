﻿using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class AdminActionController : ApiController
    {
        [HttpPost]
        public long SaveAction([FromBody] ActionParameterDTO parameters)
        {
            long actionId = -1;

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                Models.Action a = new Models.Action();
                a.ActionName = parameters.actionName;
                a.ActionParameters = parameters.parameterValues;
                a.ActionTypeId = parameters.actionTypeId;
                a.ScriptId = parameters.scriptId;
                context.Actions.Add(a);
                context.SaveChanges();
                actionId = a.ActionId;
            }



           return actionId;

   
        }

        [HttpGet]
        public IHttpActionResult getActions(long scriptId)
        {
            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var actionList = context.Actions.Where(a => a.ScriptId == scriptId).Select(s=> new { ActionId = s.ActionId, ActionType =  s.ActionType.ActionDescription, ActionName = s.ActionName, ActionParameters = s.ActionParameters }).ToList();
                return Ok(actionList);
            }
        }


    }

    public class ActionParameterDTO
    {
        public string parameterValues { get; set; }
        public long scriptId { get; set; }
        public string actionName { get; set; }
        public int actionTypeId { get; set; }
    }
}
