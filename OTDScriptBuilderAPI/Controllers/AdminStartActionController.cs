﻿using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class AdminStartActionController : ApiController
    {
        [HttpPost]
        public long SaveStartAction(long scriptId,string actionName)
        {
            long startActionId = -1;

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var action = context.Actions.Where(w => w.ScriptId == scriptId && w.ActionName == actionName).FirstOrDefault();
                if (action != null)
                {
                    ScriptStartAction sa = new ScriptStartAction();
                    sa.ActionId = action.ActionId;
                    sa.ScriptId = scriptId;
                    context.ScriptStartActions.Add(sa);
                    context.SaveChanges();
                    startActionId = sa.ScriptStartActionId;
                }
            }



           return startActionId;

   
        }

        [HttpGet]
        public IHttpActionResult GetStartAction(long scriptId)
        {
            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var res = string.Empty;
                var startAction = context.ScriptStartActions.Where(w => w.ScriptId == scriptId).FirstOrDefault();
                if (startAction!= null)
                {
                    res = startAction.Action.ActionName;
                }
                return Ok(res);
            }

        }
    }
}
