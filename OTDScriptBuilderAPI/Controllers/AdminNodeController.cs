﻿using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class AdminNodeController : ApiController
    {
        [HttpPost]
        public long SaveNode(long scriptId,string nodeName, int nodeTemplateId)
        {
            long nodeId = -1;

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                Node n = new Node();
                n.NodeName = nodeName;
                n.ScriptId = scriptId;
                n.NodeTemplateId = nodeTemplateId;
                context.Nodes.Add(n);
                context.SaveChanges();
                nodeId = n.NodeId;
            }



           return nodeId;

   
        }

        [HttpGet]
        public IHttpActionResult GetNodes(long scriptId)
        {

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var nodes = context.Nodes.Where(n => n.ScriptId == scriptId).Select(s=> new { NodeId = s.NodeId, NodeName = s.NodeName, NodeTemplateId = s.NodeTemplateId}).ToList();
                return Ok(nodes);
            }

        }
    }

}
