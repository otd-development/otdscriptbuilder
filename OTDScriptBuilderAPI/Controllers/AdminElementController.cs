﻿using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class AdminElementController : ApiController
    {
        [HttpPost]
        public long SaveElement(int elementTypeId, long nodeId,int ElementOrder)
        {
            long elementId = -1;

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                Element e = new Element();
                e.ElementTypeId = elementTypeId;
                e.NodeId = nodeId;
                e.ElementOrder = ElementOrder;
                context.Elements.Add(e);
                context.SaveChanges();
                elementId = e.ElementId;
            }



           return elementId;

   
        }

        [HttpGet]
        public IHttpActionResult getElements(long nodeId)
        {

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var elements = context.ElementParameters.Where(w => w.Element.NodeId == nodeId).Select(s => new { ElementId =  s.Element.ElementId, eType = s.Element.ElementType.ElementTypeDescription, ParameterValues = s.ParameterValues, ElementOrder=s.Element.ElementOrder }).ToList();
                elements = elements.OrderBy(o => o.ElementOrder).ToList();
                return Ok(elements);
            }






        }

    }
}
