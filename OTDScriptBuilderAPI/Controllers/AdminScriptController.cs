﻿using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Web;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class AdminScriptController : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetScripts(int entityId)
        {

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var scriptList = context.Scripts.Where(w => w.EntityId == entityId).Select(s=> new { ScriptId = s.ScriptId, ScriptName = s.ScriptName }).ToList();
                return Ok(scriptList);
            }
        }

        [HttpOptions]
        public IHttpActionResult GetOptions(int entityId)
        {
                return Ok();
        }


        [HttpPost]
        public long SaveScript(string scriptName, int entityId)
        {
            long scriptID = -1;
            bool existingRecord = false;
            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                existingRecord = context.Scripts.Where(w => w.EntityId == entityId && w.ScriptName == scriptName).Any();
                if (!existingRecord)
                {
                    Script s = new Script();
                    s.EntityId = entityId;
                    s.ScriptName = scriptName;
                    context.Scripts.Add(s);
                    context.SaveChanges();
                    scriptID = s.ScriptId;
                }
            }



           return scriptID;

   
        }

        [HttpPost]
        public void ClearScript(long scriptId)
        {

            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                var nodeList = context.Nodes.Where(n => n.ScriptId == scriptId).ToList();
                var nodeIdList = nodeList.Select(s => s.NodeId).ToList();
                var actionList = context.Actions.Where(a => a.ScriptId == scriptId).ToList();
                var elementList = context.Elements.Where(e => nodeIdList.Contains(e.NodeId));
                var elementParameterList = context.ElementParameters.Where(p => nodeIdList.Contains(p.Element.NodeId)).ToList();
                var startAction = context.ScriptStartActions.Where(w => w.ScriptId == scriptId).ToList();

                try
                {
                    context.ElementParameters.RemoveRange(elementParameterList);
                    context.SaveChanges();
                    context.Elements.RemoveRange(elementList);
                    context.SaveChanges();
                    context.Nodes.RemoveRange(nodeList);
                    context.SaveChanges();
                    context.ScriptStartActions.RemoveRange(startAction);
                    context.SaveChanges();
                    context.Actions.RemoveRange(actionList);
                    context.SaveChanges();
                }
                catch (Exception ex)
                {
                    string m = ex.Message;
                    string m2 = string.Empty;
                    if (ex.InnerException != null)
                    {
                        m2 = ex.InnerException.Message;
                    }
                }


            }






        }


    }
}
