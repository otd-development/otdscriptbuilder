﻿using Newtonsoft.Json;
using OTDScriptBuilderAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OTDScriptBuilderAPI.Controllers
{
    public class AdminElementParamtersController : ApiController
    {
        [HttpPost]
        public long SaveElementParameter([FromBody] ElementParameterDTO elements)
        {


            long elementParamterId = -1;
            using (OTDScriptsEntities context = new OTDScriptsEntities())
            {
                ElementParameter ep = new ElementParameter();
                ep.ElementId = elements.elementId;
                ep.ParameterValues = elements.parameterValues;
                context.ElementParameters.Add(ep);
                context.SaveChanges();
                elementParamterId = ep.ElementParameterId;


            }



           return elementParamterId;

   
        }


    }

    public class ElementParameterDTO
    {
        public string parameterValues { get; set; }
        public long elementId { get; set; }
    }
}
